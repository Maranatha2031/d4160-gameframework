﻿namespace DefaultGame
{
    using d4160._Camera;
    using UnityEngine;
    using d4160.Core;

    public class CameraManager : Singleton<CameraManager>, IMainCamera
    {
        public Camera Main { get => m_main; set => m_main = value; }

        [SerializeField] protected Camera m_main;

        protected void Start()
        {
            if (!m_main) m_main = Camera.main;
        }
    }
}