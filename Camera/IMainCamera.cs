﻿namespace d4160._Camera
{
    using UnityEngine;

    public interface IMainCamera
    {
        Camera Main { get; }
    }
}