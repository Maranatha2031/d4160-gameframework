﻿namespace d4160._Camera
{
    using UnityEngine;

    public interface IBoundsSize
    {
        Vector2 BoundsSize { get; }
    }
}