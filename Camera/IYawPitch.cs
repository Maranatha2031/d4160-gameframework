﻿namespace d4160._Camera
{
    public interface IYawPitch
    {
        float PitchHeigth { get; }

        float YawWidth { get; }
    }
}