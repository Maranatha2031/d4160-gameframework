﻿namespace d4160.Systems.Transition
{
    public interface ITransitionTo
    {
        void TransitionTo(float value);
    }
}
