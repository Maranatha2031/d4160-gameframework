﻿namespace d4160.Systems.Portal
{
    using UnityEngine;

    public interface IPortal
    {
        void GoThroughportal(Transform target);
    }
}