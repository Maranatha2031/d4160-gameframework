﻿namespace d4160.Systems.Launch
{
    public interface ILauncher
    {
        void Launch();
    }
}