﻿namespace d4160
{
    public interface IActivableWithLayerMask
    {
        bool IsActive { get; }

        void SetActive(bool active, int layerMask);

        // ~active & tryToActive : needActive
        // active & tryToDesactive : needDesactive
    }
}