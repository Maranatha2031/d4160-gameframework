﻿namespace d4160
{
    public interface IActivable
    {
        bool IsActive { get; }

        void SetActive(bool active);
    }
}