using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Plugins.ECS_Framework.Interfaces
{
    /// <summary>
    /// Entity who can carry a group of Items (Inventory, etc)
    /// </summary>
    interface IItemContainer
    {
    }
}
