namespace d4160.ECS.Entity
{
    using UnityEngine;

    /// <summary>
    /// aka bag, board, chest
    /// Type: Grid : IGrid<> (also for UI when use capacity (axb) to store items), List (Horizontal, Vertical, Grid Layout)
    /// View: UI, 2D (space when can't use GridLayout) or 3D Object (same as 2D) 
    /// Capacity: a x b, #, illimited
    /// Restrictions: List of Entity Refs
    /// If it's UI store as only as IItem (Icon) (IBlock with a link to a Entity for more data), returning back to the manager and with a ref to an entity, EntityType, Count (struct)
    /// If it's Object store as Entity disabling all Components (also for IItem)
    /// An Inventory (board) have Blocks (can be one for each space of the list, grid) or one reference (image, model)
    /// Each Block reference to a Piece<E> | Entity (Representation of an Item Component with a Entity reference OR directly to an Entity)
    /// Example:
    ///         CubeInventory1 : Inventory<Finite3DGrid, CubeBlock, CubePiece> (: BlueprintCubeInventory, OptionCubeInventory)
    ///                 CubeBlock[2x2x1], (with a CubePiece) Fills, (With null)ComplementFills
    ///         CubeBlock : Block<CubePiece>
    ///                 When CubePiece is not null release the Block and Put the Piece
    ///         CubePiece
    ///         ----------------------------
    ///         CubeInventory2 : Inventory<Finite3DGrid, CubePiece> <<BoardEntity>>
    ///                 CubePiece[2x2x1] (not null) Fills, (null) ComplementFills
    ///                 NullBlock (Put a null block when is null)
    ///         CubePiece
    ///         ----------------------------------------------------
    ///         Bag1 : Inventory<FiniteUIGrid, BagBlock, ItemPiece> <<ItemInventory>> even when the Item has a model (representation of the entity)
    ///             Capacity 10, BagBlock[10] with have an ItemPiece or a null ref
    ///             Only can store equipable and usable | consumable and Mineral items
    ///         BagBlock : Block<ItemPiece> { BG_Image }
    ///         ItemPiece { ItemCount, ItemComponentRef, Image, Text }
    ///         ItemComponent { Icon, Name (SetName), EntityRef, Mask from Objective(Equipable, Usable, Consumable), Mask from Entity (Mineral, Plant) }
    ///         ----------------------------
    ///         Bag2 : Inventory<FiniteUIGrid, ItemPiece>
    ///             Capacity 10, ItemPiece[10] which can be null of have a ref
    ///             NullBlock (put a null block when is null with a Image Raycast component to allow DropHandler)
    ///             Allow the null block stay in the background but without interactions? to use the BG_Image
    ///         ----------------------------
    ///         3MatchBoard : Inventory<Finite2DGrid, Piece> <<BoardEntity>>
    ///             Piece[9x9], null (ComplementFills), not null (Fills), blocked fills (to allow the grid adopt any shape)
    ///             NullBlock? or not
    ///         NormalPiece, PowerPiece1, PowerPiece2 { }
    ///         ----------------------------
    ///         Projectile Container | Entity Inventory | So this type is a COMPONENT
    /// </summary>
    public class InventoryComponent : MonoBehaviour
    {

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}