namespace d4160.ECS.Entity
{
    using UnityEngine;

    public abstract class ProjectileComponent<EData, ETypeData> //: EntityComponent<EData, ETypeData>, IProjectile where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData
    {
        public abstract Vector3 Velocity { get; set; }
    }
}
