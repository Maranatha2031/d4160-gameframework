
namespace d4160.ECS
{
    /// <summary>
    /// Finite count of Projectiles, for infinite use ProjetileAbility
    /// </summary>
    public interface IProjectileContainer// : IEntityContainer
    {
        bool HasProjectiles { get; }

    }
}
