namespace d4160.ECS.Entity
{
    using UnityEngine;

    public interface IProjectile// : IEntity
    {
        Vector3 Velocity { get; set; }
    }
}
