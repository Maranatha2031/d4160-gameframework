namespace d4160.ECS.Entity
{
    public interface IGrid1D //: IEntity
    {
        /// <summary>
        /// Don't null slots
        /// </summary>
        /// <value></value>
        int[] Fills { get; }

        int[] FillsComplement { get; }

        int GridCapacity { get; set; }

        void FillWithBlocks(bool includePositionate = false, int idx = 0);

        // Helper idx mask, can have methods with same name but different params to call this
        void PositionateAllBlocks(int idx = 0);

        void ReleaseGrid(int idx = 0);
    }

    public interface IGrid1D<B> : IGrid1D // where B : IBlock
    {
        B[] Blocks { get; }

        B this[int pos] { get; }
    }
}
