﻿namespace d4160.Systems.Grid
{
    using UnityEngine;

    public interface IReleaseGrid
    {
        void DestroyBlocks();

        void DestroyBlockAt(Vector3Int pos);
    }
}
