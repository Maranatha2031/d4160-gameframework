namespace d4160.ECS.Entity
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using d4160.Utilities;
#if UNITY_EDITOR && ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif

    public enum Axis1D
    {
        // Cases when the grid is layout with unity layout scripts
        Manual,
        X,
        Y,
        Z
    }

    /// <sumary>
    /// Warning: Remember _blocks[z, y, x] = null, the reference remains here after block = null as GC that causes random loop problems (when searching for Blocks[,,])
    /// Options: GridStateOptions (creation options, inject generally before creation)
    ///          BlockPrefabOptions (small data about the next BlockPrefab to use or to tell from manager, inject when need different BlockPrefab)
    /// </sumary>
	public abstract class Grid1DComponent<EData, ETypeData, Block> //: EntityComponent<EData, ETypeData>, IGrid1D<Block> where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData where Block : IBlock
	{
        [SerializeField]
        protected Grid1DData m_gridData;
        protected Block[] m_blocks;
        // An array to accelerate the iteration on calculus
        // Convert to List when need dynamic operations or if the new sice is fixed can use a new array[current + 1]
        protected int[] m_fills = null;
        protected int[] m_fillsComplement = null;

        public virtual Block[] Blocks {
            get {
                return m_blocks;
            }
        }
        public virtual int[] Fills {
			get {
				return m_fills;
			}
            // set this to calculate complement
            set {
                m_fills = value;
            }
		}

        public virtual int[] FillsComplement {
            get {
                return m_fillsComplement;
            }
        }

        public virtual Block this[int pos]
        {
            get {
                return m_blocks[pos];
            }
            set {
                m_blocks[pos] = value;
            }
        }

        public virtual Grid1DData GridData {
            get {
                return m_gridData;
            }
            set {
                m_gridData = value;
            }
        }
        public virtual int GridCapacity {
            get {
                return m_gridData.gridCapacity;
            }
            set {
                m_gridData.gridCapacity = value;
            }
        }
        
        public virtual void CalculateFillsComplement()
        {
            /*
            if (m_fills != null)
                m_fillsComplement = m_fills.Complement(GridCapacity);
            else
                m_fillsComplement = null;*/
        }
        
        // Helper idx mask
        // Decide what to put in the blocks (ALL) (fills the array of blocks, some can be null) (Even when use timed fill)
        // Or get from default in the inspector (Initialize call before)
        public abstract void FillWithBlocks(bool includePositionate = false, int idx = 0);
        // Actually positionate correctly in the grid (util when some grids share same blocks) 
        public virtual void PositionateAllBlocks(int idx = 0) {}
        // Try to return, hide or destroy all blocks of the grid (can choice some idx option)
        public virtual void ReleaseGrid(int idx = 0){}

        // Positionate a block in that position, can use some axis to set a default position and then move it with some tween
        public virtual void PositionateBlock(int pos, int idx = 0){}

        // Get instanced Block (from manager) for use in grid 
        protected virtual Block InstanceBlock(int idx = 0)
        { 
            return default(Block); 
        }
        protected virtual T InstanceBlock<T>(int idx = 0) where T : MonoBehaviour
        {
            //return (T)(IBlock)(InstanceBlock(idx)); 
            return default;
        }

        public virtual Block SetBlockAt(Block newBlock, int atPos, int idx = 0)
        {
            // return a block if already there is one atPos
            if (this[atPos] != null)
            {
                Block previous = this[atPos];
                this[atPos] = newBlock;

                return previous;
            }

            return default(Block);
        }

        public virtual Block GetBlockAt(int atPos, int idx = 0)
        {
            return this[atPos];
        }

        public virtual void Swap(int from, int to, int idx = 0)
        {
            Block temp = this[to];
            this[to] = this[from];
            this[from] = temp;
        }

        // Random position according to some IBlock[,,] // only for 3D! need another class to support Grid2D
        /*public static int GetAvailableRandomPosition(IBlock[] blocks, params int[] excludes)
        {
            IBlock b = null;
            int pos;
            bool thereAreExclusives = false; 
            int rdPos;
            //int max = 200, count = 0;
            do {
                pos = Random.Range(0, blocks.Length);
                b = blocks[pos];

                rdPos = pos;

                //count++;
                
                //if (count > max)
                //    break;

                if (b == null && excludes.Length > 0)
                {
                    thereAreExclusives = false;
                    for (int i = 0; i < excludes.Length; i++)
                    {
                        if (excludes[i] == rdPos)
                        {
                            thereAreExclusives = true;
                            break;
                        }
                    }
                }
            } while (b != null || thereAreExclusives);

            return rdPos;
        }*/

        public static int GetAvailableRandomPosition(int[] blockFills, int gridCapacity, int lastIdx = -1)
        {
            bool exists = false;
            int pos;
            //int maxRd = 50;
            //int count = 0;
            do {
                pos = Random.Range(0, gridCapacity);

                //Debug.Log(b);
                /* count++;
                if (count >= maxRd)
                {
                    Debug.Log("Max");
                    break;
                }*/

                exists = false;
                lastIdx = lastIdx == -1 ? blockFills.Length : lastIdx;
                for (int i = 0; i < lastIdx; i++)
                {
                    if(pos == blockFills[i])
                    {
                        exists = true;
                        break;
                    }
                }
            } while (exists);

            return pos;
        }
	}
    
    // Use a [,,] Multidimensional array to store positions
	[System.Serializable]
	public struct Grid1DData
	{
        public Axis1D axis;
		public int gridCapacity;
		public float gridSize;
		public Transform gridCenter;
        public float gridSpacing;


        public float GridCompleteSize {
            get {
                return gridCapacity * gridSize + gridSpacing * (gridCapacity - 1);
            }
        }

		public Vector3 GetWorldPosition(int pos)
        {
            float size = gridSize;
            int cap = gridCapacity;
            Vector3 centerPos = gridCenter.position;
            float localDiff = -(cap * size) / 2 + size * pos + (size / 2) + gridSpacing * pos;
            Vector3 localDiffV;
            
            switch (axis)
            {
                case Axis1D.X:
                    localDiffV = new Vector3(localDiff, 0, 0);
                break;
                case Axis1D.Y:
                    localDiffV = new Vector3(0, localDiff, 0);
                break;
                case Axis1D.Z:
                    localDiffV = new Vector3(0, 0, localDiff);
                break;
                default:
                    Debug.LogWarning("This grid is set to Manual axis, so  can't get this value.");
                    return Vector3.zero;
            }
 
            Vector3 worldDiff = gridCenter.TransformVector(localDiffV);

            return centerPos + worldDiff;
        }

        /// <sumary>
        /// Get local position as gridCenter value as center, so the parent of the object inside this grid need to be the gridCenter
        /// </sumary>
        public Vector3 GetLocalPosition(int pos)
        {
            float size = gridSize;
            int cap = gridCapacity;
            Vector3 centerPos = gridCenter.position;
            float localDiff = -(cap * size) / 2 + size * pos + (size / 2) + gridSpacing * pos;
            Vector3 localDiffV;
            
            switch (axis)
            {
                case Axis1D.X:
                    localDiffV = new Vector3(localDiff, 0, 0);
                break;
                case Axis1D.Y:
                    localDiffV = new Vector3(0, localDiff, 0);
                break;
                case Axis1D.Z:
                    localDiffV = new Vector3(0, 0, localDiff);
                break;
                default:
                    Debug.LogWarning("This grid is set to Manual axis, so  can't get this value.");
                    return Vector3.zero;
            }

            return localDiffV;
        }
	}
}