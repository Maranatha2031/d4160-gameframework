﻿namespace d4160.Systems.Grid
{
    using UnityEngine;

    /// <sumary>
    /// Warning: Remember _blocks[z, y, x] = null, the reference remains here after block = null as GC that causes random loop problems (when searching for Blocks[,,])
    /// Options: GridStateOptions (creation options, inject generally before creation)
    ///          BlockPrefabOptions (small data about the next BlockPrefab to use or to tell from manager, inject when need different BlockPrefab)
    /// </sumary>
	public abstract class Grid3D<Block> : MonoBehaviour where Block : class
	{
        [SerializeField] protected Grid3D m_gridData;
        protected Block[,,] m_blocks;

        public virtual Grid3D GridData => m_gridData;
        public virtual Block[,,] Blocks => m_blocks;

        public virtual Block this[Vector3Int pos]
        {
            get {
                if (m_blocks != null)
                    return m_blocks[pos.z, pos.y, pos.x];

                return default;
            }
            set {
                if (m_blocks != null)
                    m_blocks[pos.z, pos.y, pos.x] = value;
            }
        }

        public virtual Block this[int x, int y, int z]
        {
            get {
                if (m_blocks != null)
                    return m_blocks[z, y, x];

                return default;
            }
            set {
                if (m_blocks != null)
                    m_blocks[z, y, x] = value;
            }
        }

        public virtual Vector3Int GridCapacity
        {
            get => m_gridData.gridCapacity;
            set => m_gridData.gridCapacity = value;
        }

        public virtual Vector3 GridSize
        {
            get => m_gridData.gridSize;
            set => m_gridData.gridSize = value;
        }

        public virtual Vector3 GridSpacing
        {
            get => m_gridData.gridSpacing;
            set => m_gridData.gridSpacing = value;
        }

        public virtual Transform GridCenter
        {
            get => m_gridData.gridCenter;
            set => m_gridData.gridCenter = value;
        }

        protected abstract void PositionateBlock(int x, int y, int z);

        protected virtual void PositionateBlock(Vector3Int v)
        {
            PositionateBlock(v.x, v.y, v.z);
        }

        protected abstract Block InstanceBlock(int idx = 0);

        protected virtual T InstanceBlock<T>(int idx = 0) where T : class
        {
            return InstanceBlock(idx) as T; 
        }

        public static Vector3Int GetAvailableRandomPosition(Block[,,] blocks, params Vector3[] excludes)
        {
            Block b = null;
            int rdX, rdY, rdZ;
            bool thereAreExclusives = false; 
            Vector3Int rdV;
            //int max = 200, count = 0;
            do {
                rdZ = Random.Range(0, blocks.GetLength(0));
                rdY = Random.Range(0, blocks.GetLength(1));
                rdX = Random.Range(0, blocks.GetLength(2));
                b = blocks[rdZ, rdY, rdX];

                rdV = new Vector3Int(rdX, rdY, rdZ);

                //count++;
                
                //if (count > max)
                //    break;

                if (b == null && excludes.Length > 0)
                {
                    thereAreExclusives = false;
                    for (int i = 0; i < excludes.Length; i++)
                    {
                        if (excludes[i] == rdV)
                        {
                            thereAreExclusives = true;
                            break;
                        }
                    }
                }
            } while (b != null || thereAreExclusives);

            return rdV;
        }

        public static Vector3Int GetAvailableRandomPosition(Vector3Int[] blockFills, Vector3Int gridCapacity, int indexLimit = -1)
        {
            Vector3Int b;
            bool exists = false;
            int rdX, rdY, rdZ;
            //int maxRd = 50;
            //int count = 0;
            do {
                rdZ = Random.Range(0, (int)gridCapacity.z);
                rdY = Random.Range(0, (int)gridCapacity.y);
                rdX = Random.Range(0, (int)gridCapacity.x);
                b = new Vector3Int(rdX, rdY, rdZ);

                //Debug.Log(b);
                /* count++;
                if (count >= maxRd)
                {
                    Debug.Log("Max");
                    break;
                }*/

                exists = false;
                indexLimit = indexLimit == -1 ? blockFills.Length : indexLimit;
                for (int i = 0; i < indexLimit; i++)
                {
                    if(b == blockFills[i])
                    {
                        exists = true;
                        break;
                    }
                }
            } while (exists);

            return b;
        }
	}
}