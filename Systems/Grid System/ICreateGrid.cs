﻿namespace d4160.Systems.Grid
{
    using UnityEngine;

    public interface ICreateGrid
    {
        void CreateBlocks(Vector3Int[] fills = null);

        void CreateBlockAt(Vector3Int pos, int type = 0);
    }
}
