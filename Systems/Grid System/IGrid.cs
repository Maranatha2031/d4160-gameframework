﻿namespace d4160.ECS.Entity
{
    using UnityEngine;

    public interface IGrid //: IEntity
    {
        Vector3Int[] Fills { get; }

        Vector3Int[] FillsComplement { get; }

        Vector3Int GridCapacity { get; set; }

        void FillWithBlocks(bool includePositionate = false, int idx = 0);

        // Helper idx mask, can have methods with same name but different params to call this
        void PositionateBlocks(int idx = 0);
        void ReleaseGrid(int idx = 0);
    }

    public interface IGrid<B> : IGrid  //where B : IBlock
    {
        B[,,] Blocks { get; }

        B this[Vector3Int pos] { get; }
    }
}
