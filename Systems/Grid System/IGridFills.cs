﻿namespace d4160.Systems.Grid
{
    using UnityEngine;

    public interface IGridFills
    {
        Vector3Int[] Fills { get; set; }

        Vector3Int[] FillsComplement { get; }
    }
}
