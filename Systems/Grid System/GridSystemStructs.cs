﻿namespace d4160.Systems.Grid
{
    using d4160.Core;
    using UnityEngine;

    [System.Serializable]
    public struct Grid3D
    {
        public Vector3Int gridCapacity;
        public Vector3 gridSize;
        public Transform gridCenter;
        public Vector3 gridSpacing;

        public Vector3 GridCompleteSize
        {
            get
            {
                // Never with x, y, z in zero
                Vector3 comSize = new Vector3(
                    gridCapacity.x * gridSize.x + gridSpacing.x * (gridCapacity.x - 1),
                    gridCapacity.y * gridSize.y + gridSpacing.y * (gridCapacity.y - 1),
                    gridCapacity.z * gridSize.z + gridSpacing.z * (gridCapacity.z - 1)
                );

                return comSize;
            }
        }

        public float GriCompleteSizeX => gridCapacity.x * gridSize.x + gridSpacing.x * (gridCapacity.x - 1);

        public float GriCompleteSizeY => gridCapacity.y * gridSize.y + gridSpacing.y * (gridCapacity.y - 1);

        public float GriCompleteSizeZ => gridCapacity.z * gridSize.z + gridSpacing.z * (gridCapacity.z - 1);

        public Vector3 GetWorldPosition(Vector3Int idxV)
        {
            return GetWorldPosition(idxV.x, idxV.y, idxV.z);
        }

        public Vector3 GetWorldPosition(int x, int y, int z)
        {
            Vector3 size = gridSize;
            Vector3Int cap = gridCapacity;
            Vector3 centerPos = gridCenter.position;

            Vector3 localDiff = new Vector3(
                -(cap.x * size.x) / 2 + size.x * x + (size.x / 2) + gridSpacing.x * x,
                -(cap.y * size.y) / 2 + size.y * y + (size.y / 2) + gridSpacing.y * y,
                -(cap.z * size.z) / 2 + size.z * z + (size.z / 2) + gridSpacing.z * z
            );
            Vector3 worldDiff = gridCenter.TransformVector(localDiff);

            return centerPos + worldDiff;
        }

        public Vector3 GetLocalPosition(Vector3Int idxV)
        {
            return GetLocalPosition(idxV.x, idxV.y, idxV.z);
        }

        /// <sumary>
        /// Get local position as gridCenter value as center, so the parent of the object inside this grid need to be the gridCenter
        /// </sumary>
        public Vector3 GetLocalPosition(int x, int y, int z)
        {
            Vector3 size = gridSize;
            Vector3Int cap = gridCapacity;
            Vector3 centerPos = gridCenter.position;

            Vector3 localDiff = new Vector3(
                -(cap.x * size.x) / 2 + size.x * x + (size.x / 2) + gridSpacing.x * x,
                -(cap.y * size.y) / 2 + size.y * y + (size.y / 2) + gridSpacing.y * y,
                -(cap.z * size.z) / 2 + size.z * z + (size.z / 2) + gridSpacing.z * z
            );

            return localDiff;
        }

        public int GridCapacityScalar
        {
            get
            {
                return gridCapacity.Scalar();
            }
        }
    }
}
