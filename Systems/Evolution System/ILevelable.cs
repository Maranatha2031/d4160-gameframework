﻿
namespace d4160
{
    /// <summary>
    /// Interface to work with entities that have DifficultySet, RewardSet, Lever or other leveleable components 
    /// </summary>
    public interface ILeveleable 
	{
        int CurrentLevel { get; }

		void LevelUp(int amount = 1);

		void LevelDown(int amount = 1);

		void LevelTo(int level);
	}
}