namespace d4160.ECS.Component
{
    using d4160.Scriptables;

    /// <summary>
    /// Profile to store traits and data of some level of dificulty (normal, easy, hard) for some entity (also include stats and new entity render forms)
    /// </summary>
    public abstract class LeverProfileScriptable //: ProfileScriptable
    {

    }
}