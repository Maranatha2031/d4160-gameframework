namespace d4160.ECS.Component
{
    using UnityEngine;

    public interface ILever //: IComponent
    {
        Vector2Int LevelLimits { get; }

        int CurrentLevel { get; }

        ILevelOptions CurrentLevelOptions { get; }
    }

    // One Phase of score inside a level (Level 1, 2, 3...) as plain data to send and use
    public interface ILevelOptions //: IOptions
    {
        
    }
    
    // One Phase of level inside a level (for level 1, 2, 3...) as stored data, used when there is a mix between Traits and other values, so other values store here 
    public interface ILevelPhase
    {
        
    }
}