namespace d4160
{
    public interface ILevelFollower
    {
        /// <summary>
        /// When this Entity (related to this Interface) references some GameState in GameMode
        /// </summary>
        int LevelIdx
        {
            get;
            set;
        }
    }
}
