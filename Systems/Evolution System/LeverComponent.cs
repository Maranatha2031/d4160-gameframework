namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// Component to work with all kinds of Leveleables (Stats, Change of render forms) except Difficulty, Rewards, Objective, Score components that have its own level logic
    /// Can use Stats (Level, Experience, ExpReward) to control data
    /// This component is the one that need to have the real Level value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class LeverComponent<T> //: ComponentDataWrapper<T>, ILever where T : IComponentData
    {
        protected int m_currenLevel = 1;
        protected Vector2Int m_levelLimits = Vector2Int.one;

        public virtual Vector2Int LevelLimits { 
            get {
                return m_levelLimits;
            }
            set
            {
                m_levelLimits = value;
            }
        }

        public virtual int CurrentLevel {
            get {
                return m_currenLevel;
            }
            set {
                m_currenLevel = value;
                if (value < m_levelLimits.x)
                    m_currenLevel = m_levelLimits.x;
                else if (value > m_levelLimits.y)
                    m_currenLevel = m_levelLimits.y;
            }
        }

        public abstract ILevelOptions CurrentLevelOptions
        {
            get;
        }

        /// <summary>
        /// Use this method if there is some data in this object to set limits 
        /// </summary>
        public virtual void SetDefaultLevelLimits()
        {
        }
    }
}