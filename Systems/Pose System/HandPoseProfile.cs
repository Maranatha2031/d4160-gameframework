namespace d4160.ECS.Entity
{
    using UnityEngine;

    /// <summary>
    /// To store one pose shape structure for IPoseables.null IPoses and IPosers
    //[CreateAssetMenu(fileName = "HandPose Profile", menuName = "Scriptable/Profile/Pose/FiveFingers Hand")]
    /// </summary>
    public class HandPoseProfile : ThreeFingersHandPoseProfile
    {
        //public FingerPoseData finger4;
        //public FingerPoseData finger5;

        public override int FingersNumber {
            get {
                return 5;
            }
        }

        /*public override FingerPoseData GetFinger(int fingerId)
        {
            switch(fingerId)
            {
				case 1:
					return finger1;
				case 2:
					return finger2;
				case 3:
					return finger3;
                case 4:
					return finger4;
				case 5:
					return finger5;
			}

			return default(FingerPoseData);
        }*/
    }
}