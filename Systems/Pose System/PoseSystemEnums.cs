﻿namespace d4160.Systems.Pose
{
    public enum HandType
    {
        Any,
        Left,
        Right
    }
}