﻿namespace d4160.ECS.Component
{
    // For Entities that have PoseSets
    public interface IPoseSetContainer// : IObject
    {
        //IPose GetPose(int poserIdx, int idx);

        //PoseProfileScriptable GetPoseProfile(int poserIdx, int idx);

        /// <summary>
        /// Try to get the PoseSet of the index, so this way support many PoseSet in one entity, but the Poseables need to know its idx
        /// </summary>
        /// <param name="idx"></param>
        /// <returns></returns>
        //IPoseSet GetPoseSet(int idx = 0);
    }
}