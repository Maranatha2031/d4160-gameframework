namespace d4160.ECS.Component
{
    using UnityEngine;
    using Core;
    using Scriptables;

    /* Pose type for this particular Pose, as index for P[], so start with 0 */
    /*public enum __PoseType
    {
        OnGrab,
        OnTouch
    }*/
    /// <summary>
    /// Component to store a group of poses for a special PoseProfile
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TPd"></typeparam>
    /// <typeparam name="TP"></typeparam>
    public abstract class PoseSetComponent<T, TPd, TP> //: ComponentDataWrapper<T>, IPoseSet<TPd, TP> where T : IComponentData where TPd : ProfilePoseSet<TP> where TP : IPose
    {
        /// </sumary>
        [SerializeField]
        protected TPd m_poserData;

        public TPd PoserData => m_poserData;

        public PoseProfileScriptable PoseProfile => default;//m_poserData.poseProfile;

        public virtual TP GetPose(int index = 0)
        {
            //return m_poserData.poses.IsValidIndex(index) ? m_poserData.poses[index] : default;
            return default;
        }

        public virtual CP GetPose<CP>(int index = 0) where CP : IPose
        {
            /*if (m_poserData.poses.IsValidIndex(index))
            {
                return (CP)((IPose)m_poserData.poses[index]);
            }*/

            return default;
        }

        public virtual IPose GetIPose(int index = 0)
        {
            //return m_poserData.poses.IsValidIndex(index) ? m_poserData.poses[index] : default(IPose);
            return default;
        }
    }

    /// <summary>
    /// The inner pose
    /// </summary>
    /// <typeparam name="P"></typeparam>
    [System.Serializable]
    public class ProfilePoseSet<P> where P : IPose
    {
        public PoseProfileScriptable poseProfile;
        public P[] poses;
    }

    // For use to config Animation layers weights and IK weights (FinalIk for example)
    [System.Serializable]
    public struct PoseWeight
    {
        public float weight;
        [Tooltip("Can denied this finger to move on freezed pose: IPoseable.SetPose(pose, true)")]
        public bool freezed;
    }
}
