﻿namespace d4160.Systems.Pose
{
    [System.Serializable]
    public struct HandLayers
    {
        public int thumbLayer;
        public int indexLayer;
        public int middleLayer;
        public int ringLayer;
        public int pinkyLayer;
    }
}