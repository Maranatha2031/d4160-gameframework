namespace d4160.Scriptables
{
    using d4160.ECS.Entity;

    /// <summary>
    /// To store one pose shape structure for IPoseables.null IPoses and IPosers
    //[CreateAssetMenu(fileName = "PoseProfile", menuName = "Scriptable/Profile/Pose/___")]
    /// </summary>
    public class PoseProfileScriptable //: ProfileScriptable
    {
        /* Facade for finger poses */
        public virtual int FingersNumber {
            get {
                return 0;
            }
        }
        /*public virtual FingerPoseData GetFinger(int fingerId)
        {
            return default;
        }*/
    }
}