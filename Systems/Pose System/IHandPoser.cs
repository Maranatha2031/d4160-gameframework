namespace d4160.Systems.Pose
{ 
    using UnityEngine;

    public interface IHandPoserGeneric<TPose>  where TPose : ScriptableObject
    {
        TPose GetPoseAt(int idx = 0);
    }
}