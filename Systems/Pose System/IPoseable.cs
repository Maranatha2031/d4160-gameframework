namespace d4160
{
    /*public enum __PoseType
{
OnGrab,
OnTouch
}*/

    using d4160.ECS.Component;
    using d4160.ECS;
    using d4160.Scriptables;

    /// <summary>
    /// For motion that a allow poses, also can be for the Poseable Entity
    /// When create a poseable entity, think about the PoseSet(Profile and Pose) and the allowed PoseTypes, also add the new PoseSet in the PosetType list or enum (PoseDefinition)
    /// </summary>
    public interface IPoseable
    {
        IPose CurrentPose { get; }

        PoseProfileScriptable PoseProfile { get; }

        // freezePose inside IPose true: can't modify pose values until ResetPose is called 
        void SetPose(PoseProfileScriptable profile, IPose pose);//, int poseSetIdx = 0);

        /* To restore it to default pose */
        void ResetPose();
    }

    public interface IHandPoseable : IPoseable
    {
        /// <summary>
        /// Grip the finger to a fixed weight 
        /// </summary>
        /// <param name="fingerId">Same as Finger enum</param>
        /// <param name="weight">The fixed weight to grip (IK or Animation weight layer)</param>
        /// <param name="tween">If use a tween to change the weight (util for simulation, Oculus TouchControls don't need this)</param>
        void Grip(int fingerId, float weight, bool tween = false);
    }

    /// <summary>
    /// Configuration for a PoseProfile like ThreeFingersHandPose, etc
    /// </summary>
    public interface IPose //: IObject
    {
        PoseWeight GetWeight(int id);

        bool FreezePose
        {
            get;
        }
    }
}