namespace d4160.ECS.Component
{
    /*public enum ThreeFingersHandPoseType
    {
        OnGrab,
        OnTouch
    }*/

    using Entity;

    /// <summary>
    /// The default PoseSet for ThreeFingersHands
    /// Create a similar class to create a custom one
    /// </summary>
    public class ThreeFingersHandPoseSetComponent// : PoseSetComponent<ThreeFingersHandPoseSet, ThreeFingersHandProfilePoseSet, ThreeFingersHandPose>
	{
	}

	[System.Serializable]
	public struct ThreeFingersHandPoseSet //: IComponentData
	{
	}

    [System.Serializable]
	public class ThreeFingersHandProfilePoseSet //: ProfilePoseSet<ThreeFingersHandPose>
	{
	}
}