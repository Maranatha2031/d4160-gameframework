namespace d4160.ECS.Entity
{
    using d4160.Scriptables;
    using UnityEngine;

    /// <summary>
    /// To store one pose shape structure for IPoseables.null IPoses and IPosers
    //[CreateAssetMenu(fileName = "ThreeFingersHandPose Profile", menuName = "Scriptable/Profile/Pose/ThreeFingers Hand")]
    /// </summary>
    public class ThreeFingersHandPoseProfile : PoseProfileScriptable
    {
        /*public FingerPoseData finger1;
        public FingerPoseData finger2;
        public FingerPoseData finger3;*/

        public override int FingersNumber {
            get {
                return 3;
            }
        }

        /*public override FingerPoseData GetFinger(int fingerId)
        {
            switch(fingerId)
            {
				case 1:
					return finger1;
				case 2:
					return finger2;
				case 3:
					return finger3;
			}

			return default(FingerPoseData);
        }*/
    }
}