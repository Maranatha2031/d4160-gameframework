using d4160.ECS.Entity;

namespace d4160.ECS.Component
{
    /// <summary>
    /// The default PoseSet for Hands
    /// Create a similar class to create a custom one
    /// </summary>
    public class HandPoseSetComponent //: PoseSetComponent<HandPoseSet, HandProfilePoseSet, HandPose>
	{
	}

	[System.Serializable]
	public struct HandPoseSet //: IComponentData
	{
	}

    [System.Serializable]
	public class HandProfilePoseSet //: ProfilePoseSet<HandPose>
	{
	}
}