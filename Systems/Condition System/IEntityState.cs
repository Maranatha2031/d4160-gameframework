namespace d4160.GameFramework
{
    public interface IEntityState
    {
        /// <summary>
        /// Runtime (current) state of the entity
        /// </summary>
        int EntityState { get; }
        /// <summary>
        /// Is this Entity pooled by its Manager?
        /// </summary>
        //bool CancelFirstOnEnable { get; set; }

        //void Initialize();

        //void SetActive(bool active, int partsLayerMask = 0);
    }
}
