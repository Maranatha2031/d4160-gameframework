namespace d4160.Systems.Time
{
    [System.Serializable]
    public struct TimerData
    {
        public float multiplier;
        public TimerDirection tDirection;
    }
}