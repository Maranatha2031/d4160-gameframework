namespace d4160.Systems.Time
{
    public interface IStopTimer
    {
        void StopTimer(bool stop, int idx = -1);
    }
}