namespace d4160.Systems.Time
{
    public enum TimerDirection
    {
        None,
        Decrement,
        Increment
    }
}