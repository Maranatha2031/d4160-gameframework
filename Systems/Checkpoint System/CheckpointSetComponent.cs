namespace d4160.ECS.Component
{
    /// <summary>
    /// For Entities (Game or World) to store a set of Checkpoints to allow return, instead of invisible points, can be a proper Checkpoint Entity (entities for the unique objective to be a Checkpoint)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CheckpointSetComponent<T>// : ComponentDataWrapper<T> where T : IComponentData
    {
    }
}
