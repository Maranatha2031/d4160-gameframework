namespace d4160.ECS.Component
{
    using Entity;

    /// <summary>
    /// Generally only want one per entity
    /// </summary>
    /// <typeparam name="TData"></typeparam>
    public abstract class InputComponent<TData, TEntity> //: EntityLinkComponent<TData, TEntity>, IInput where TData : IComponentData where TEntity : IEntity
    {
    }
}
