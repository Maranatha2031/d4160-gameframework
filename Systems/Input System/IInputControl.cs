namespace d4160.ECS.Component
{
    /// <summary>
    /// For Entities that allow control inputs
    /// </summary>
    public interface IInputControl //: IObject
    {
        /// <summary>
        /// From the available input scripts that we have, the actived one (maybe don't needed in the new InputSystem)
        /// </summary>
        int SelectedInputScriptIdx { get; }
        void SetActiveInputComponent(bool active, int inputLayerMask = 0);
    }
}
