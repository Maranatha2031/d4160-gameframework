namespace d4160.ECS.Component
{
    public abstract class InputScriptable<I> : ComponentScriptable<I> where I : IInput
    {
    }
}