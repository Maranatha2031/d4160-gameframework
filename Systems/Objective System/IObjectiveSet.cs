﻿namespace d4160.ECS.Component
{
    using UnityEngine;

    public interface IObjectiveSet //: IComponent
    {
        Vector2Int LevelLimits { get; }

        int CurrentLevel { get; }

        IObjectiveOptions CurrentObjectiveOptions { get; }
    }

    // One Phase of difficulty inside a level (for level 1, 2, 3...) as plain data to send and use
    // To store as part of the Component
    public interface IObjectiveOptions 
    {

    }

    // One Phase of difficulty inside a level (for level 1, 2, 3...) as stored data, used when there is a mix between Traits and other values, so other values store here 
    // To store as part of the scriptable
    public interface IObjectivePhase
    {

    }
}