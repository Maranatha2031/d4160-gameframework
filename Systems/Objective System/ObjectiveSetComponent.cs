namespace d4160.ECS.Component
{
    using UnityEngine;

    // Store ObjectiveProfiles (Normal, Easy, Hard, etc)
    public abstract class ObjectiveSetComponent<T> //: ComponentDataWrapper<T>, IObjectiveSet where T : IComponentData
    {
        protected int m_currenLevel = 0;
        protected Vector2Int m_levelLimits = Vector2Int.zero;

        public virtual Vector2Int LevelLimits
        {
            get
            {
                return m_levelLimits;
            }
            set
            {
                m_levelLimits = value;
            }
        }

        public virtual int CurrentLevel
        {
            get
            {
                return m_currenLevel;
            }
            set
            {
                m_currenLevel = value;
                if (value < m_levelLimits.x)
                    m_currenLevel = (int)m_levelLimits.x;
                else if (value > m_levelLimits.y)
                    m_currenLevel = (int)m_levelLimits.y;
            }
        }

        public abstract IObjectiveOptions CurrentObjectiveOptions
        {
            get;
        }
        // Traits
        // DifficultyProfiles

        // Used when there is a default level limits
        public virtual void SetDefaultLevelLimits()
        { }
    }
}