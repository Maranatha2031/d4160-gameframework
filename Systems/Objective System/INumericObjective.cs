﻿namespace d4160.Systems.Objective
{
    public interface INumericObjective
    {
        int ObjectiveNumber { get; }

        int CorrectTrials { get; }

        int IncorrectTrials { get; }

        void AddObjectiveTrial(bool correct, int value = 1);
    }
}
