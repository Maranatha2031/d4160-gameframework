﻿namespace d4160.Systems.Objective
{
    using System;
    using UnityEngine;

    public abstract class ObjectiveBase : ScriptableObject
    {
        public virtual string Name => name;

        public abstract bool IsObjectiveComplete { get; }

        public abstract void CalculateObjective(int difficultyLevel);
    }
}
