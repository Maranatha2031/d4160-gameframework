
namespace d4160.ECS
{
    /// <summary>
    /// Entity who has Buffs to modify another entity
    /// </summary>
    public interface IBuffModifierContainer //: IObject
    {
    }
}