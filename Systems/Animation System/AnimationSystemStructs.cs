﻿namespace d4160.Systems.Animation
{
    using UnityEngine;

    [System.Serializable]
    public struct AnimatorParam
    {
        public string paramName;

        private int _paramHash;

        public int ParamHash => _paramHash;

        public void CalculateHash()
        {
            if (!string.IsNullOrEmpty(paramName))
            {
                _paramHash = Animator.StringToHash(paramName);
            }
            else
            {
                Debug.LogError($"AnimatorAction.CalculateHash(): Error paramName is empty");
            }
        }
    }

    [System.Serializable]
    public struct AnimatorWeight
    {
        public int layer;
        [Range(0, 1f)]
        public float weight;
    }

    [System.Serializable]
    public struct AnimatorAction
    {
        public string actionName;
        public string paramName;
        public AnimatorParamType paramType;
        public string paramValue;

        private int _paramHash;
        private int _actionNameHash;
        private int _intValue;
        private bool _boolValue;
        private float _floatValue;

        public int ParamHash => _paramHash;
        public int IntValue => _intValue;
        public bool BoolValue => _boolValue;
        public float FloatValue => _floatValue;

        public void CalculateHash()
        {
            if (!string.IsNullOrEmpty(paramName))
            {
                _paramHash = Animator.StringToHash(paramName);
                _actionNameHash = Animator.StringToHash(actionName);

                ConvertParamValue();
            }
            else
            {
                Debug.LogError($"AnimatorAction.CalculateHash(): Error paramName is empty");
            }
        }

        public bool CompareHash(int hash)
        {
            return _actionNameHash == hash;
        }

        public void SetAction(Animator anim)
        {
            switch (paramType)
            {
                case AnimatorParamType.Int:
                    anim.SetInteger(_paramHash, _intValue);
                    break;
                case AnimatorParamType.Float:
                    anim.SetFloat(_paramHash, _floatValue);
                    break;
                case AnimatorParamType.Bool:
                    anim.SetBool(_paramHash, _boolValue);
                    break;
                case AnimatorParamType.Trigger:
                    anim.SetTrigger(_paramHash);
                    break;
                default:
                    break;
            }
        }

        private void ConvertParamValue()
        {
            bool result = false;
            switch (paramType)
            {
                case AnimatorParamType.Int:
                    result = int.TryParse(paramValue, out _intValue);
                    break;
                case AnimatorParamType.Float:
                    result = float.TryParse(paramValue, out _floatValue);
                    break;
                case AnimatorParamType.Bool:
                    result = bool.TryParse(paramValue, out _boolValue);
                    break;
                case AnimatorParamType.Trigger:
                    result = true;
                    break;
                default:
                    break;
            }

            if (!result)
            {
                Debug.LogError($"AnimatorAction.ConvertParamValue(): Error trying parse {paramValue} paramValue of type {paramType}");
            }
        }
    }
}
