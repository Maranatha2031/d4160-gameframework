namespace d4160.ECS.Component
{
    public interface IAnimation //: IComponent
    {
        AnimationType AnimationType
        {
            get;
        }

        bool IsActive {
            get;
        }

        bool CanUse { get; }

        void PlayAnimation();

        void StopAnimation();
    }
}
