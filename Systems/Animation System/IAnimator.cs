
namespace d4160.ECS.Component
{
    public interface IAnimator //: IComponent
    {
        void SetFloat(int paramHash, float value);
    }
}
