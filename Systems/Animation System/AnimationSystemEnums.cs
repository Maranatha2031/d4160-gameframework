﻿namespace d4160.Systems.Animation
{
    public enum AnimatorParamType
    {
        Int,
        Float,
        Bool,
        Trigger
    }
}
