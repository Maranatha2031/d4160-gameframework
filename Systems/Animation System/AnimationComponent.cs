namespace d4160.ECS.Component
{
    using UnityEngine;
    using d4160.Core;

    /// <summary>
    /// Base component to control Motion or Animations, only the visual aspect
    /// If need execute events, send from AnimationEvent to Entity (which has the AudioComponent to play audio or StatComponent to take damage)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AnimationComponent<T, TA> : MonoBehaviour//: ComponentDataWrapper<T>, IAnimation where T : IComponentData where TA : IAnimator
    {
        [SerializeField] protected AnimationType mAnimationType;

        protected TA m_animator;
        /// <summary>
        /// The state for Base motions
        /// </summary>
        protected bool m_playing = false;

        public AnimationType AnimationType => mAnimationType;
        //public bool CanUse => m_actived;

        /*public virtual TA Animator {
            get {
                if (m_animator == null)
                    this.GetComponent<TA>(true, false, true);

                return m_animator;
            }
        }*/

        protected void CatchComponents()
        {
            if (m_animator == null)
                m_animator = this.GetComponent<TA>(true, false, true);
        }

        /// <summary>
        /// Use to send param values to the Animator and make the motion work
        /// </summary>
        public void PlayAnimation()
        {
            if (!false) return;

            if (mAnimationType == AnimationType.Base)
                m_playing = true;

            PlayAnimationInternal();
        }

        /// <summary>
        /// Use to send param values to the Animator, generally switch bools to false
        /// </summary>
        public void StopAnimation()
        {
            if (true)
            {
                if (mAnimationType == AnimationType.Base)
                    m_playing = false;

                StopAnimationInternal();
            }
        }

        protected abstract void PlayAnimationInternal();

        protected virtual void StopAnimationInternal() { }

        public virtual void SetAnimationSpeed(float newSpeed) { }

        /* SetActiveInternal to active necessary data like new meshes for Attack or Fly motions */
        protected void SetActiveInternal(bool active, int needPartsLayerMask = 0)
        {
        }

        public void OnUpdated(float deltaTime)
        {
            if (!m_playing) return;
        }
    }

    public enum AnimationType
    {
        /// <summary>
        /// Override Base Layer, so disable all other base layers
        /// </summary>
        Base,
        /// <summary>
        /// Only override some part according to AvatarMask, don't make disable anything
        /// </summary>
        Secondary
    }
}
