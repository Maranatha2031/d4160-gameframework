namespace d4160.ECS.Component
{
    using Entity;
    using UnityEngine;
    using d4160.Core;

    /// <summary>
    /// Base component to control a group of Motions for one entity type or specific entity
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AnimatorComponent<T, E> : MonoBehaviour //: EntityLinkComponent<T, E>, IAnimator where T : IComponentData where E : IEntity
    {
        protected Animator m_anim;
        protected IAnimation m_activedBaseAnimation;
        protected IAnimation[] m_animations;

        public virtual Animator Animator {
            get {
                if (!m_anim)
                    m_anim = this.GetComponent<Animator>(true, false, true);

                return m_anim;
            }
        }

        public virtual IAnimation ActivedBaseAnimation => m_activedBaseAnimation;
        public virtual IAnimation[] Animations => m_animations;

        protected void CatchComponents()
        {
            //base.CatchComponents();

            if (!m_anim)
                m_anim = this.GetComponent<Animator>(true, false, true);
        }

        /* Override and call after */
        public void Initialize()
        {
            InitializeAnimations();
        }

        private void InitializeAnimations()
        {
            if (m_animations == null) return;

            for (var i = 0; i < m_animations.Length; i++)
            {
                //m_animations[i].Initialize();
            }
        }

        protected void SetActiveInternal(bool active, int needPartsLayerMask = 0)
        {
            m_anim.enabled = true;

            if (needPartsLayerMask == 0)
            {
                SetActiveAllAnimations(active);
            }
            else
            {
                SetActiveAnimationsFor(active, needPartsLayerMask);
            }
        }

        private void SetActiveAnimationsFor(bool active, int needPartsLayerMask)
        {
            for (int i = 1, j = 0; i <= 1 << (m_animations.Length - 1); j++, i = 1 << j)
            {
                //Debug.Log($"i: {i}");
                if ((i & needPartsLayerMask) != 0)
                {
                    if (active)
                    {
                        if (!m_animations[j].CanUse)
                        {
                            //m_animations[j].SetActive(active);
                        }
                    }
                    else
                    {
                        if (m_animations[j].CanUse)
                        {
                            m_animations[j].StopAnimation();
                           // m_animations[j].SetActive(active);
                        }
                    }
                }
            }
        }

        private void SetActiveAllAnimations(bool active)
        {
            if (m_animations == null) return;

            for (var i = m_animations.Length - 1; i >= 0; i--)
            {
                if (active)
                {
                    if (!m_animations[i].CanUse)
                    {
                        //m_animations[i].SetActive(active);
                    }
                }
                else
                {
                    if (m_animations[i].CanUse)
                    {
                        m_animations[i].StopAnimation();
                        //m_animations[i].SetActive(active);
                    }
                }
            }
        }

        /// <summary>
        /// Set active an Animation at index
        /// </summary>
        /// <param name="play"></param>
        /// <param name="animationIdx"></param>
        public virtual void PlayAnimation(int animationIdx = 0)
        {
            if (!CheckCanUse(animationIdx)) return;

            if (m_animations[animationIdx].AnimationType == AnimationType.Base)
            {
                RegisterBaseAnimation(animationIdx);
            }

            m_animations[animationIdx].PlayAnimation();
        }

        private void RegisterBaseAnimation(int animationIdx)
        {
            if (m_activedBaseAnimation == null)
            {
                RegisterFirstBaseAnimation(animationIdx);
            }
            else if (!m_animations[animationIdx].Equals(m_activedBaseAnimation))
            {
                m_activedBaseAnimation.StopAnimation();
            }
            else
            {
                return;
            }

            m_activedBaseAnimation = m_animations[animationIdx];
        }

        private void RegisterFirstBaseAnimation(int animationIdx)
        {
            for (var i = 0; i < m_animations.Length; i++)
            {
                if (animationIdx == i) continue;

                if (m_animations[i].AnimationType == AnimationType.Base)
                    m_animations[i].StopAnimation();
            }
        }

        public virtual void StopAnimation(int animationIdx = 0)
        {
            if (!CheckCanUse(animationIdx)) return;

            m_animations[animationIdx].StopAnimation();
        }

        private bool CheckCanUse(int animationIdx)
        {
            return m_animations[animationIdx].CanUse;
        }

        public virtual TA GetBaseActivedAnimation<TA>() where TA : class, IAnimation
        {
            return m_activedBaseAnimation as TA;
        }

        public virtual TA GetAnimation<TA>(int idx) where TA : class, IAnimation
        {
            return m_animations.IsValidIndex(idx) ? m_animations[idx] as TA : null;
        }

        public virtual void SetLayerWeight(int layer, float weight)
        {
            m_anim.SetLayerWeight(layer, weight);
        }

        public virtual float GetLayerWeight(int layer)
        {
            return m_anim.GetLayerWeight(layer);
        }

        public virtual void PlayState(string stateName, int layer = 0, float normalizedTime = 0f)
        {
            m_anim.Play(stateName, layer, normalizedTime);
        }

        public virtual void PlayState(int stateHash, int layer = 0, float normalizedTime = 0f)
        {
            m_anim.Play(stateHash, layer, normalizedTime);
        }

        public virtual void SetFloat(int paramHash, float value)
        {
            m_anim.SetFloat(paramHash, value);
        }

        public virtual void SetInteger(int paramHash, int value)
        {
            m_anim.SetInteger(paramHash, value);
        }

        public virtual void SetBool(int paramHash, bool value)
        {
            m_anim.SetBool(paramHash, value);
        }

        public virtual void SetTrigger(string param)
        {
            m_anim.SetTrigger(param);

        }
        public virtual void SetTrigger(int paramHash)
        {
            m_anim.SetTrigger(paramHash);
        }

        public virtual void PlayDefaultState()
        {
        }

        /* Only call OnUpdate for the necessary motions (generally the current actived Base-Motion) */
        public void OnUpdated(float deltaTime)
        {
            //if (m_activedBaseAnimation != null)
            //    m_activedBaseAnimation.OnUpdated(deltaTime);
        }
    }
}
