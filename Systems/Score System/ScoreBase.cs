﻿namespace d4160.Systems.Score
{
    using UnityEngine;

    public abstract class ScoreBase : ScriptableObject
    {
        public abstract void CalculateScore(int difficultyLevel);
    }
}