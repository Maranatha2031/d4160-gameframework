namespace d4160.ECS.Component
{
    using UnityEngine;

    public abstract class RewardSetComponent<T> //: ComponentDataWrapper<T>, IRewardSet where T : IComponentData
    {
        protected int m_currenLevel = 1;
        protected Vector2Int m_levelLimits = Vector2Int.one;
        
        public virtual Vector2Int LevelLimits { 
            get {
                return m_levelLimits;
            } 
            set {
                m_levelLimits = value;
            }
        } 

        public virtual int CurrentLevel {
            get {
                return m_currenLevel;
            }
            set {
                m_currenLevel = value;
                if (value < m_levelLimits.x)
                    m_currenLevel = m_levelLimits.x;
                else if (value > m_levelLimits.y)
                    m_currenLevel = m_levelLimits.y;
            }
        }

        public abstract IRewardOptions CurrentRewardOptions
        {
            get;
        }

        // Used when there is a default level limits
        public virtual void SetDefaultLevelLimits()
        {}
    }
}