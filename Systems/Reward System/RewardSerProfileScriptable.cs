namespace d4160.ECS.Component
{
    using d4160.Scriptables;

    /// <summary>
    /// To store traits and score values
    /// As for DifficultyLevel (Hard, Easy, Normal)
    /// If don't use values for levels (1, 2...) don't need array or levelTrait (one for each level)
    //[CreateAssetMenu(fileName = "__RewardProfile", menuName = "Scriptable/Profile/Reward/___")]
    /// </summary>
    public abstract class RewardSetProfileScriptable //: ProfileScriptable
    {

    }
}