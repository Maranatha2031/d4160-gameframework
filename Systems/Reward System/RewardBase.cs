﻿namespace d4160.Systems.Reward
{
    using UnityEngine;

    public abstract class RewardBase : ScriptableObject
    {
        public abstract void CalculateReward(int difficultyLevel);
    }
}
