namespace d4160.ECS.Entity
{
    /// <summary>
    /// It's a group of Places (Zones)
    /// Can store references of Entities foreach Zones or for all World, theWorld detects entities OnTriggerEnter and Exit
    /// Has a Trigger by default to cover the World
    /// Has one or more Suns (directional lights), when is only Realtime and detect some active ICamera enter, then Tween the WorldManager DirectionalLight reference to the Light of this world. When need more lights, create it in the Manager. Always Disable the Realtime Lights at the start and set the Manager's ones.
    /// Can have LightSet component, so have lights in the world
    /// Relate a list of Scenes (world tiles)
    /// </summary>
    public abstract class WorldComponent<EData, ETypeData> //: EntityComponent<EData, ETypeData>, IWorld where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData
    {

    }
}