﻿
namespace d4160.ECS.Component
{
    /// <summary>
    /// When the Entity has the StatSet component
    /// </summary>
    public interface IStatContainer<S> : IStatContainer where S : IStat
    {
        S[] Stats { get; }
    }

    public interface IStatContainer //: IObject
    { }
}
