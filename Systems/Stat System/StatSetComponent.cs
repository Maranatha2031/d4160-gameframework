namespace d4160.ECS.Component
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using d4160.Scriptables;

    public abstract class StatSetComponent<T, S, SM> where S: IStat where SM: IStatModifier
    {
        /// <summary>
        /// After SetModifier or Add or Remove Temporal Modifiers
        /// </summary>
       // public Action<IStat> onStatUpdate;
        public event Action<IStat> onStatUpdate;

        /// <summary>
        /// The Main Stat (prefer Vital), so if becomes zero try to call Destroy in the Entity
        /// </summary>
        public virtual S MainStat {
            get {
                return Stats[0];
            }
        }

        /// <summary>
        /// Ensure to init the List and set Value as BaseValue of each Stat (SetDefaults method)
        /// </summary>
        public abstract S[] Stats { get; }

        public virtual bool IsAlive {
            get {
                return MainStat.IsAlive;
            }
        }

        public virtual bool IsMax {
            get {
                return MainStat.IsMax;
            }
        }

        public void Initialize()
        {
            for (int i = 0; i < Stats.Length; i++)
            {
                Stats[i].SetDefaults();
            }
        }

        public virtual S GetStat(int idx)
        {
            return Stats[idx];
        }

        public virtual void SetStat(S v, int idx = 0)
        {
            Stats[idx] = v;
        }

        public virtual void AddModifiers(IStatModifierContainer<SM> statModCon)
        {
            for (int i = 0; i < Stats.Length; i++)
            {
                SM[] statMods = statModCon.StatMods;
                bool hasTemporalMod = false;

                for (int j = 0; j < statMods.Length; j++)
                {
                    if (statMods[j].StatId == Stats[i].Id)
                    {
                        if (statMods[j].ModType == StatModType.Temporal)
                        {
                            Stats[i].AddModifier(statMods[j]);
                            hasTemporalMod = true;
                        }
                        else
                            Stats[i].SetModifier(statMods[j], onStatUpdate);

                        if (j == statMods.Length - 1 && hasTemporalMod)
                            Stats[i].CalculateFinalValues(onStatUpdate);
                    }
                }
            }
        }

        public virtual void RemoveModifiers(IStatModifierContainer<SM> statModCon)
        {
            for (int i = 0; i < Stats.Length; i++)
            {
                SM[] statMods = statModCon.StatMods;
                bool needCalculateFinalvalues = false;

                for (int j = 0; j < statMods.Length; j++)
                {
                    if (statMods[j].StatId == Stats[i].Id)
                    {
                        if (statMods[j].ModType == StatModType.Temporal)
                        {
                            // Even is a struct can remove correctly
                            needCalculateFinalvalues = Stats[i].RemoveModifier(statMods[j]);
                        }

                        if (j == statMods.Length - 1 && needCalculateFinalvalues)
                            Stats[i].CalculateFinalValues(onStatUpdate);
                    }
                }
            }
        }
    }

    [Serializable]
    public struct Stat_Default : IStat
    {
        /// <summary>
        /// The value related to StatScriptable one
        /// </summary>
        public StatDefinition_Default id;
        public StatType type;
        // Base Values (Level 1)
        [Header("Base Values")]
        public float bValue;
        public float bMaxValue;
        public float bMinValue;
        [Header("True Values")]
        // True Values
        [SerializeField] private float _tValue;
        [SerializeField] private float _tMaxValue;
        [SerializeField] private float _tMinValue;
        [Header("Permanent Values")]
        // Permanent Values, aditional after the calculus of True Values
        [SerializeField] private float _perValue;
        [SerializeField] private float _perMax;
        [SerializeField] private float _perMin;
        // Last mod values, to calculate the % just do (lastvalueMod / (_tValue - lastvalueMod))
        private float _lastValueMod;
        private float _lastMaxMod;
        private float _lastMinMod;
        // Flags for the need to calculate again True Values
        private bool _needCalculateCurrentFinal;
        private bool _needCalculateMaxFinal;
        private bool _needCalculateMinFinal;
        private bool _ignorePerValues;
        // For the calculus of True Values from Base Values
        private List<IStatModifier> _tValueMods;
        private List<IStatModifier> _tMaxMods;
        private List<IStatModifier> _tMinMods;

        public int Id { get { return (int)id; } }
        public StatType StatType { get { return type; } }
        public float BaseValue { get { return bValue; } }
        public float BaseMaxValue { get { return bMaxValue; } }
        public float BaseMinValue { get { return bMinValue; } }
        public float TrueValue { get { return _tValue; } }
        public float TrueMaxValue { get { return _tMaxValue; } }
        public float TrueMinValue { get { return _tMinValue; } }
        public float LastValueMod { get { return _lastValueMod; } }
        public float LastMaxMod { get { return _lastMaxMod; } }
        public float LastMinMod { get { return _lastMinMod; } }
        public string Name { get { return id.ToString(); } }
        public List<IStatModifier> CurrentValueMods { get { return _tValueMods; } }
        public List<IStatModifier> MaxValueMods { get { return _tMaxMods; } }
        public List<IStatModifier> MinValueMods { get { return _tMinMods; } }

        public bool IsAlive
        {
            get
            {
                return _tValue > bMinValue;
            }
        }

        public bool IsMax
        {
            get
            {
                return _tValue == bMaxValue;
            }
        }

        public bool IgnorePermanentValues {
            get {
                return _ignorePerValues;
            }
            set {
                _ignorePerValues = value;
            }
        }

        public void SetDefaults()
        {
            _tValue = bValue;
            _tMinValue = bMinValue;
            _tMaxValue = bMaxValue;

            _tValueMods = new List<IStatModifier>(0);
            _tMaxMods = new List<IStatModifier>(0);
            _tMinMods = new List<IStatModifier>(0);
        }

        private void ResetLastModValues()
        {
            _lastMaxMod = 0;
            _lastMinMod = 0;
            _lastValueMod = 0;
        }

        /// <summary>
        /// For Instant and Permanent Modifiers
        /// </summary>
        /// <param name="mod"></param>
        public void SetModifier(IStatModifier mod, Action<IStat> onUpdated = null)
        {
            if (mod.ModType == StatModType.Temporal)
                return;

            ResetLastModValues();

            CalculateFinalValueFor(mod);

            onUpdated?.Invoke(this);
        }

        private void CalculateFinalValueFor(IStatModifier mod)
        {
            float trueValue = 0, finalValue = 0, modFinalValue = 0;

            switch (mod.PropertyType)
            {
                case StatModPropertyType.Current:
                    trueValue = _tValue;
                    break;
                case StatModPropertyType.Max:
                    trueValue = _tMaxValue;
                    break;
                case StatModPropertyType.Min:
                    trueValue = _tMinValue;
                    break;
                default:
                    break;
            }

            if (mod.CalculusType == StatModCalculusType.Flat)
            {
                finalValue = trueValue + mod.Value;
            }
            else
            {
                switch (mod.CalculusTargetType)
                {
                    case StatModCalculusTargetType.BaseCurrent:
                        finalValue = bValue;
                        break;
                    case StatModCalculusTargetType.BaseMax:
                        finalValue = bMaxValue;
                        break;
                    case StatModCalculusTargetType.BaseMin:
                        finalValue = bMinValue;
                        break;
                    case StatModCalculusTargetType.TrueCurrent:
                        finalValue = _tValue;
                        break;
                    case StatModCalculusTargetType.TrueMax:
                        finalValue = _tMaxValue;
                        break;
                    case StatModCalculusTargetType.TrueMin:
                        finalValue = _tMinValue;
                        break;
                    default:
                        break;
                }

                finalValue *= 1 + mod.Value;
            }

            bool addFinal = mod.CalculusType == StatModCalculusType.Flat ? false : mod.AddToFinalCalculus;
            bool storePermanent = mod.ModType == StatModType.Permanent;

            switch (mod.PropertyType)
            {
                case StatModPropertyType.Current:
                    modFinalValue = addFinal ? finalValue : finalValue - _tValue;

                    _tValue = addFinal ? _tValue + finalValue : finalValue;
                    _tMinValue += (_ignorePerValues ? 0 : _perValue);

                    _perValue += storePermanent ? modFinalValue : 0;
                    _lastValueMod = modFinalValue;

                    _tValue = Mathf.Clamp(_tValue, _tMinValue, _tMaxValue);
                    break;
                case StatModPropertyType.Max:
                    modFinalValue = addFinal ? finalValue : finalValue - _tMaxValue;

                    _tMaxValue = addFinal ? _tMaxValue + finalValue : finalValue;
                    _tMaxValue += (_ignorePerValues ? 0 : _perMax);

                    _perMax += storePermanent ? modFinalValue : 0;

                    _lastMaxMod = modFinalValue;
                    _lastValueMod = mod.FollowMax ? modFinalValue : 0;

                    _tValue += _lastValueMod;
                    _tValue = Mathf.Clamp(_tValue, _tMinValue + 1, _tMaxValue);
                    break;
                case StatModPropertyType.Min:
                    modFinalValue = addFinal ? finalValue : finalValue - _tMinValue;

                    _tMinValue = addFinal ? _tMinValue + finalValue : finalValue;
                    _tMinValue += (_ignorePerValues ? 0 : _perMin);

                    _perMin += storePermanent ? modFinalValue : 0;
                    _lastMinMod = modFinalValue;

                    _tValue = Mathf.Clamp(_tValue, _tMinValue + 1, _tMaxValue);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// For Temporal modifiers
        /// </summary>
        /// <param name="mod"></param>
        public void AddModifier(IStatModifier mod)
        {
            if (mod.ModType != StatModType.Temporal)
                return;

            //if (statMods == null) statMods = new List<IStatModifier>();
            switch (mod.PropertyType)
            {
                case StatModPropertyType.Current:
                    _needCalculateCurrentFinal = true;
                    _tValueMods.Add(mod);
                    break;
                case StatModPropertyType.Max:
                    _needCalculateMaxFinal = true;
                    _tMaxMods.Add(mod);
                    break;
                case StatModPropertyType.Min:
                    _needCalculateMinFinal = true;
                    _tMinMods.Add(mod);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// For Temporal Modifiers
        /// </summary>
        /// <param name="mod"></param>
        public bool RemoveModifier(IStatModifier mod)
        {
            if (mod.ModType != StatModType.Temporal)
                return false;

            switch (mod.PropertyType)
            {
                case StatModPropertyType.Current:
                    if (_tValueMods.Remove(mod))
                    {
                        _needCalculateCurrentFinal = true;
                        return true;
                    }
                    break;
                case StatModPropertyType.Max:
                    if (_tMaxMods.Remove(mod))
                    {
                        _needCalculateMaxFinal = true;
                        return true;
                    }
                    break;
                case StatModPropertyType.Min:
                    if (_tMinMods.Remove(mod))
                    {
                        _needCalculateMinFinal = true;
                        return true;
                    }
                    break;
                default:
                    break;
            }

            return false;
        }

        public void RemoveAllModifiersFromContainer(IStatModifierContainer container)
        {
            int numRemovals = _tValueMods.RemoveAll(mod => mod.Container == container);

            if (numRemovals > 0)
                _needCalculateCurrentFinal = true;

            numRemovals = _tMaxMods.RemoveAll(mod => mod.Container == container);

            if (numRemovals > 0)
                _needCalculateMaxFinal = true;

            numRemovals = _tMinMods.RemoveAll(mod => mod.Container == container);

            if (numRemovals > 0)
                _needCalculateMinFinal = true;
        }

        /// <summary>
        /// Sort in order ASC
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
		private int CompareModifierOrder(IStatModifier a, IStatModifier b)
        {
            if (a.Order < b.Order)
                return -1;
            else if (a.Order > b.Order)
                return 1;
            return 0; //if (a.Order == b.Order)
        }

        /// <summary>
        /// Calculate the final value after each modifier, ensure to call this after any modifier for an Entity
        /// </summary>
        public void CalculateFinalValues(Action<IStat> onUpdated = null)
        {
            ResetLastModValues();

            CalculateCurrentFinal();

            CalculateMaxFinal();

            CalculateMinFinal();

            onUpdated?.Invoke(this);
        }

        private void CalculateCurrentFinal()
        {
            if (!_needCalculateCurrentFinal)
                return;

            _needCalculateCurrentFinal = false;

            float lastValue = _tValue;
            float finalValue = bValue;
            float sumPercentAdd = 0;

            _tValueMods.Sort(CompareModifierOrder);

            for (int i = 0; i < _tValueMods.Count; i++)
            {
                IStatModifier mod = _tValueMods[i];

                if (mod.CalculusType == StatModCalculusType.Flat)
                {
                    finalValue += mod.Value;
                }
                else if (mod.CalculusType == StatModCalculusType.PercentAdd)
                {
                    sumPercentAdd += mod.Value;

                    if (i + 1 >= _tValueMods.Count || _tValueMods[i + 1].CalculusType != StatModCalculusType.PercentAdd)
                    {
                        finalValue *= 1 + sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                }
                else if (mod.CalculusType == StatModCalculusType.PercentMult)
                {
                    finalValue *= 1 + mod.Value;
                }
            }

            // Workaround for float calculation errors, like displaying 12.00001 instead of 12
            finalValue = (float)Math.Round(finalValue, 4);

            _lastValueMod = finalValue - lastValue;

            finalValue += _ignorePerValues ? 0 : _perValue;

            _tValue = Mathf.Clamp(finalValue, _tMinValue, _tMaxValue);
        }

        private void CalculateMaxFinal()
        {
            if (!_needCalculateMaxFinal)
                return;

            _needCalculateMaxFinal = false;

            float lastMaxValue = _tMaxValue;
            float lastValue = _tValue;
            float finalValue = bMaxValue;
            float sumPercentAdd = 0;
            float valueModFinal = 0;

            _tMaxMods.Sort(CompareModifierOrder);

            for (int i = 0; i < _tMaxMods.Count; i++)
            {
                IStatModifier mod = _tMaxMods[i];

                if (mod.CalculusType == StatModCalculusType.Flat)
                {
                    finalValue += mod.Value;
                }
                else if (mod.CalculusType == StatModCalculusType.PercentAdd)
                {
                    sumPercentAdd += mod.Value;

                    if (i + 1 >= _tMaxMods.Count || _tMaxMods[i + 1].CalculusType != StatModCalculusType.PercentAdd)
                    {
                        finalValue *= 1 + sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                }
                else if (mod.CalculusType == StatModCalculusType.PercentMult)
                {
                    finalValue *= 1 + mod.Value;
                }

                valueModFinal += mod.FollowMax ? finalValue - valueModFinal - lastValue : 0;
            }

            // Workaround for float calculation errors, like displaying 12.00001 instead of 12
            finalValue = (float)Math.Round(finalValue, 4);

            _lastMaxMod = finalValue - lastMaxValue;
            // If don't have any mod, the _lastValueMod is calculated from the finalMax - LastValue
            valueModFinal = valueModFinal == 0 ? finalValue - lastValue : valueModFinal;
            _lastValueMod = valueModFinal;

            _tMaxValue = finalValue + (_ignorePerValues ? 0 : _perMax);

            _tValue += valueModFinal;
            _tValue = Mathf.Clamp(_tValue, _tMinValue + 1, _tMaxValue);
        }

        private void CalculateMinFinal()
        {
            if (!_needCalculateMinFinal)
                return;

            _needCalculateMinFinal = false;

            float lastMinValue = _tMinValue;
            float finalValue = bMinValue;
            float sumPercentAdd = 0;

            _tMinMods.Sort(CompareModifierOrder);

            for (int i = 0; i < _tMinMods.Count; i++)
            {
                IStatModifier mod = _tMinMods[i];

                if (mod.CalculusType == StatModCalculusType.Flat)
                {
                    finalValue += mod.Value;
                }
                else if (mod.CalculusType == StatModCalculusType.PercentAdd)
                {
                    sumPercentAdd += mod.Value;

                    if (i + 1 >= _tMinMods.Count || _tMinMods[i + 1].CalculusType != StatModCalculusType.PercentAdd)
                    {
                        finalValue *= 1 + sumPercentAdd;
                        sumPercentAdd = 0;
                    }
                }
                else if (mod.CalculusType == StatModCalculusType.PercentMult)
                {
                    finalValue *= 1 + mod.Value;
                }
            }

            // Workaround for float calculation errors, like displaying 12.00001 instead of 12
            finalValue = (float)Math.Round(finalValue, 4);

            _lastMinMod = finalValue - lastMinValue;

            _tMinValue = finalValue + (_ignorePerValues ? 0 : _perMin);

            _tValue = Mathf.Clamp(_tValue, _tMinValue + 1, _tMaxValue);
        }
    }
}
