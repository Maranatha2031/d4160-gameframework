﻿namespace d4160.Scriptables
{
    using d4160.GameFramework;

    /// <summary>
    /// <para>A list(enum) of possible Stat Types.</para>
    /// <para>Frequently used to quickly lookup a type of stat on a character.</para>
    /// </summary>
    public enum StatDefinition_Default
    {
        Health = 1, Mana, RegenHp, RegenMp, Agility, Dexterity, Endurance, Strength
    }

    // For LevelComponent Level, Experience, ExpReward

    //[CreateAssetMenu(fileName = "StatScriptable", menuName = "Scriptable/StatScriptable")]
    /// <summary>
    /// For more advanced storage of extra data like Icon, Localization values, etc
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class StatScriptable<T> //: ArchetypesSO<T> where T : DefaultArchetype, new()
	{

	}

    public enum StatType
    {
        Vital,
        Stat,
        //Currency?, Ammunition?
    }
}