namespace d4160.ECS.Component
{
    /// <summary>
    /// When the Entity have StatModifiers
    /// </summary>
    public interface IStatModifierContainer<SM> : IStatModifierContainer where SM: IStatModifier
    {
        /// <summary>
        /// Ensure to set the Container value of each StatModifier specially when is a temporal modifier
        /// </summary>
        SM[] StatMods { get; }
    }

    public interface IStatModifierContainer //: IObject
    {
    }
}
