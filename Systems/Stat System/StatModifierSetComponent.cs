﻿namespace d4160.ECS.Component
{
    using d4160.Scriptables;
    using UnityEngine;

    public abstract class StatModifierSetComponent<T, SM> //: ComponentDataWrapper<T>, IStatModifierContainer<SM> where T : IComponentData where SM : IStatModifier
    {
        public abstract SM[] StatMods { get; }
    }

    [System.Serializable]
    public struct StatModifier_Default : IStatModifier
    {
        /// <summary>
        /// The value related to StatScriptable one
        /// </summary>
        public StatDefinition_Default statId;
        public StatModPropertyType propertyType;
        public StatModCalculusType calculusType;
        [Tooltip("Temporal for Add and Remove uses (Buffs and Items). Instant for Healths and Damages. Permanent for some peculiar cases (Don't use for Vitals Current because never becomes 0!)")]
        public StatModType modType;
        public float value;
        public IStatModifierContainer container;
        [Header("Max property only")]
        [Tooltip("When modify the Max property, the Current Value also follow its value? e.g. (Increment 50% of the max health, so the current health also increments that value)")]
        public bool followMax;
        [Header("Instant and Permanent only, not Flat")]
        [Tooltip("The Target to modify from, Base values can't be modified by Modifiers but can be used as value to choice from (bValue = 50, tValue = 100, Increment 50% from which?).")]
        public StatModCalculusTargetType calculusTargetType;
        [Tooltip("Instead of make equality from the final value, add it. e.g.(Heal 50% of the max health, so need to calculate the 50% of the max health and them add it to the current value)")]
        public bool addToFinalCalculus;

        public int StatId { get { return (int)statId; } }
        public StatModCalculusType CalculusType { get { return calculusType; } }
        public StatModType ModType { get { return modType; } }
        public StatModCalculusTargetType CalculusTargetType { get { return calculusTargetType; } }
        public StatModPropertyType PropertyType { get { return propertyType; } }
        public float Value { get { return value; } }
        public int Order { get { return (int)calculusType; } }
        public bool FollowMax { get { return followMax; } }
        public IStatModifierContainer Container { get { return container; } }
        public bool AddToFinalCalculus { get { return addToFinalCalculus; } }
    }

    public enum StatModCalculusType
    {
        Flat = 100,
        PercentAdd = 200,
        PercentMult = 300
    }

    public enum StatModType
    {
        /// <summary>
        /// So affect the Stat and can take it out (add and remove)
        /// Useful for Buffs, Debuffs and Items
        /// </summary>
        Temporal,
        /// <summary>
        /// Only has an instant temporal effect in the target (don't save as PermanentValue) but also can't be removed until TrueValues are calculated again
        /// Useful for Healths and Damages
        /// </summary>
        Instant,
        /// <summary>
        /// Has a permanent effect in the target (save as PermanentValue)
        /// Is cases when need a permanent bonus (switch this with IgnorePermanentValues)
        /// </summary>
        Permanent
    }

    /// <summary>
    /// Use this as 100% to calculate True Values in Percent calculus types
    /// </summary>
    public enum StatModCalculusTargetType
    {
        BaseCurrent,
        BaseMax,
        BaseMin,
        TrueCurrent,
        TrueMax,
        TrueMin
    }

    public enum StatModPropertyType
    {
        /// <summary>
        /// The current value 
        /// </summary>
        Current,
        /// <summary>
        /// The max value
        /// </summary>
        Max,
        /// <summary>
        /// The min value
        /// </summary>
        Min
    }
}
