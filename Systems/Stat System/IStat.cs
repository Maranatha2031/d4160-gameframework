﻿namespace d4160.ECS.Component
{
    using d4160.Scriptables;
    using System;
    using System.Collections.Generic;

    public interface IStat //: IObject
    {
        int Id { get; }
        StatType StatType { get; }
        bool IsAlive { get; }
        bool IsMax { get; }
        float TrueValue { get; }
        float TrueMaxValue { get; }
        float TrueMinValue { get; }
        float BaseValue { get; }
        float BaseMaxValue { get; }
        float BaseMinValue { get; }
        float LastValueMod { get; }
        float LastMaxMod { get; }
        float LastMinMod { get; }
        string Name { get; }
        List<IStatModifier> CurrentValueMods { get; }
        List<IStatModifier> MaxValueMods { get; }
        List<IStatModifier> MinValueMods { get; }

        void SetDefaults();

        void SetModifier(IStatModifier mod, Action<IStat> onUpdated);

        void AddModifier(IStatModifier mod);

        bool RemoveModifier(IStatModifier mod);

        void RemoveAllModifiersFromContainer(IStatModifierContainer container);

        void CalculateFinalValues(Action<IStat> onUpdated);
    }

    public interface IStatModifier //: IObject
    {
        int StatId { get; }
        StatModCalculusType CalculusType { get; }
        StatModType ModType { get; }
        StatModCalculusTargetType CalculusTargetType { get; }
        StatModPropertyType PropertyType { get; }
        float Value { get; }
        int Order { get; }
        bool FollowMax { get; }
        IStatModifierContainer Container { get; }
        bool AddToFinalCalculus { get; }
    }
}