﻿namespace d4160.Systems.Level
{
    using UnityEngine;

    public interface ILevelLimits
    {
        Vector2Int LevelLimits { get; }
    }
}
