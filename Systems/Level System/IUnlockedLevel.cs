﻿namespace d4160.Systems.Level
{
    public interface IUnlockedLevel
    {

        int HighestUnlockedLevel { get; set; }
    }
}
