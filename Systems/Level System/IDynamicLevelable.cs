﻿namespace d4160.Systems.Level
{
    public interface IDynamicLevelable
    {
        void AddCorrectTrials(int value = 1);

        void AddIncorrectTrials(int value = 1);
    }
}
