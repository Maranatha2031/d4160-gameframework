﻿namespace d4160.Systems.Level
{
    using UnityEngine;

    public abstract class LevelableBase : ScriptableObject
    {
        public virtual int StartingLevel { get { return 1; } set { } }

        public abstract void LevelUp(int amount = 1);

        public abstract void LevelDown(int amount = 1);

        public virtual void LevelTo(int level) { }
    }
}
