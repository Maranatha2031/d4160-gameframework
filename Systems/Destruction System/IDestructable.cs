﻿namespace d4160.Systems.Destruction
{
    public interface IDestructable
    {
        void Destroy();
    }
}