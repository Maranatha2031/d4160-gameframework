﻿namespace DefaultGame
{
    public interface IColliders
    {
        void SetActiveColliders(bool active);
    }
}