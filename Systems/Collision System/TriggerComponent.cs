#if UNITY_PHYSICS
using UnityEngine;

namespace d4160.ECS.Component
{
    [RequireComponent(typeof(Collider))]
    public abstract class TriggerComponent<T> : MonoBehaviour// : ComponentDataWrapper<T>, ITrigger where T : IComponentData
    {
        protected Collider m_collider;

        public virtual Collider Collider
        {
            get
            {
                return m_collider;
            }
        }

        protected  void CatchComponents()
        {
            m_collider = GetComponent<Collider>();
        }
    }
}
#endif