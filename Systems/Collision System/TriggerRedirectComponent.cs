#if UNITY_PHYSICS
namespace d4160.ECS.Component
{
    using UnityEngine;

	// T can be some definition data like Head, Arm, for Action games when need different damage calculation on hit them
	public abstract class TriggerRedirectComponent<T, ITEvents> : MonoBehaviour// : ComponentDataWrapper<T> where T : IComponentData where ITEvents : ITriggerEvents
	{
		[SerializeField]
		[Tooltip("Is this is null, try to find Interface on parent GO")]
		protected GameObject entityTargetGO;
		protected ITEvents m_eventsReceiver;

		protected void CatchComponents()
		{
			if (!entityTargetGO)
				m_eventsReceiver = GetComponentInParent<ITEvents>();
			else
				m_eventsReceiver = entityTargetGO.GetComponent<ITEvents>();
		}
	}
}
#endif