#if UNITY_PHYSICS2D
namespace d4160.ECS.Component
{
    using UnityEngine;

    // T can be some definition data like Head, Arm, for Action games when need different damage calculation on hit them
    public abstract class Trigger2DRedirectComponent<T, TIEvents>// : ComponentDataWrapper<T> where T : IComponentData where TIEvents : ITrigger2DEvents
	{
		[SerializeField]
		[Tooltip("Is this is null, try to find Interface on parent GO")]
		protected GameObject entityTargetGO;
		protected TIEvents m_eventsReceiver;

		protected void CatchComponents()
		{
			/*if (!entityTargetGO)
				m_eventsReceiver = GetComponentInParent<TIEvents>();
			else
				m_eventsReceiver = entityTargetGO.GetComponent<TIEvents>();*/
		}
	}
}
#endif