using UnityEngine;

namespace d4160
{
#if UNITY_PHYSICS2D

    public interface ITrigger2DEvents : ICoreTriggerEvents
    {
    }

    public interface IFullTrigger2DEvents : ITrigger2DEvents
    {
        void OnTriggerEnter2DCallback(Collider2D other);

        void OnTriggerExit2DCallback(Collider2D other);

        void OnTriggerStay2DCallback(Collider2D other);
    }

    public interface ISingle1Trigger2DEvents : ITrigger2DEvents
    {
        void OnTriggerEnter2DCallback(Collider2D other);
    }

    public interface ISingle2Trigger2DEvents : ITrigger2DEvents
    {
        void OnTriggerExit2DCallback(Collider2D other);
    }

    public interface IBasicTrigger2DEvents : ITrigger2DEvents
    {
        void OnTriggerEnter2DCallback(Collider2D other);

        void OnTriggerExit2DCallback(Collider2D other);
    }
#endif

    public interface ICoreTriggerEvents
    {
        // Like name, part of body, etc
        // extraData is the TriggerRedirect Component itself
        void SendExtra(MonoBehaviour extraData);
    }
}