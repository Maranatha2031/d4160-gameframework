#if UNITY_PHYSICS2D
namespace d4160.ECS.Component
{
    using UnityEngine;

    [RequireComponent(typeof(Collider2D))]
    public abstract class Trigger2DComponent<T>// : ComponentDataWrapper<T>, ITrigger where T : IComponentData
    {
        protected Collider2D m_collider2D;

        public virtual Collider2D Collider2D
        {
            get
            {
                //if (!m_collider2D)
                //    m_collider2D = GetComponent<Collider2D>();

                return m_collider2D;
            }
        }

        protected void CatchComponents()
        {
            //if (!m_collider2D)
            //    m_collider2D = GetComponent<Collider2D>();
        }
    }
}
#endif