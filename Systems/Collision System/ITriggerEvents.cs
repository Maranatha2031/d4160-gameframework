#if UNITY_PHYSICS
using UnityEngine;

namespace d4160
{
    public interface ITriggerEvents : ICoreTriggerEvents
    {
    }

    public interface IFullTriggerEvents : ITriggerEvents
    {
        void OnTriggerEnterCallback(Collider other);

        void OnTriggerExitCallback(Collider other);

        void OnTriggerStayCallback(Collider other);
    }

    public interface ISingle1TriggerEvents : ITriggerEvents
    {
        void OnTriggerEnterCallback(Collider other);
    }

    public interface ISingle2TriggerEvents : ITriggerEvents
    {
        void OnTriggerExitCallback(Collider other);
    }

    public interface IBasicTriggerEvents : ITriggerEvents
    {
        void OnTriggerEnterCallback(Collider other);

        void OnTriggerExitCallback(Collider other);
    }
}
#endif