﻿namespace d4160.Systems.Look
{
    using UnityEngine;

    public interface ILookAtVector3
    {
        void LookAt(Vector3 target);
    }

    public interface ILookAtVector2
    {
        void LookAt(Vector2 target);
    }

    public interface ILookAtTransform
    {
        void LookAt(Transform target);
    }
}