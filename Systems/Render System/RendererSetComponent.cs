namespace d4160.ECS.Component
{
    using UnityEngine;
    using d4160.Core;

	/// <summary>
	/// Component that has useful methods to work with Renderers
	/// </summary>
	public abstract class RendererSetComponent<T> : MonoBehaviour//: ComponentDataWrapper<T> where T : IComponentData
	{
		[SerializeField] protected Renderer[] m_renderers;
		[Header("Custom")]
		[Tooltip("Change to this using ChangeToMaterial(idx) method")]
		[SerializeField] protected Material[] m_customMaterials;
		[Tooltip("Change to this using ChangeToShader(idx) method")]
        [SerializeField] protected Shader[] m_customShaders;

        public virtual Renderer[] Renderers => m_renderers;

        protected void SetActiveInternal(bool active, int needPartsLayerMask = 0)
        {
            for (int i = 0; i < m_renderers.Length; i++)
            {
                m_renderers[i].enabled = active;
            }
        }

        public Material GetInstancedMaterial(int renIdx = 0, int matIdx = 0)
		{
			if (m_renderers.IsValidIndex(renIdx))
			{
				if (matIdx == 0)
					return m_renderers[renIdx].material;

				if (m_renderers[renIdx].materials.IsValidIndex(matIdx))
					return m_renderers[renIdx].materials[matIdx];
			}

			return null;
		}

		public Material GetSharedMaterial(int renIdx = 0, int matIdx = 0)
		{
			if (m_renderers.IsValidIndex(renIdx))
			{
				if (matIdx == 0)
					return m_renderers[renIdx].sharedMaterial;

				if (m_renderers[renIdx].sharedMaterials.IsValidIndex(matIdx))
					return m_renderers[renIdx].sharedMaterials[matIdx];
			}

			return null;
		}

		public Shader GetShader(int renIdx = 0, int matIdx = 0)
		{
			if (m_renderers.IsValidIndex(renIdx))
			{
				if (matIdx == 0)
					return m_renderers[renIdx].material.shader;

				if (m_renderers[renIdx].sharedMaterials.IsValidIndex(matIdx))
					return m_renderers[renIdx].materials[matIdx].shader;
			}

			return null;
		}

		public void SetActiveRenderer(int renIdx, bool active)
		{
			if (m_renderers.IsValidIndex(renIdx))
			{
				m_renderers[renIdx].enabled = active;
			}
		}

		/// <summary>
        /// Use this to get renderers on objects being instanced
        /// </summary>
        /// <param name="parent"></param>
		public virtual void SearchRenderers(Transform parent = null)
		{
			if (parent)
                m_renderers = parent.GetComponentsInChildren<Renderer>();
            else
                m_renderers = this.GetComponentsInChildren<Renderer>();
		}

		public virtual void ChangeMaterial(Material mat, int renIdx = 0, int matIdx = 0)
		{
			if (renIdx == -1)
			{
				for (int i = 0; i < m_renderers.Length; i++)
				{
					if (matIdx == 0)
					{
						m_renderers[i].material= mat;
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[i].materials.Length; j++)
						{
							m_renderers[i].materials[j] = mat;
						}
					}
					else
					{
						if (m_renderers[i].materials.IsValidIndex(matIdx))
							m_renderers[i].materials[matIdx] = mat;
					}
				}
			}
			else
			{
				if (m_renderers.IsValidIndex(renIdx))
				{
					if (matIdx == 0)
					{
						m_renderers[renIdx].material = mat;
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[renIdx].materials.Length; j++)
						{
							m_renderers[renIdx].materials[j] = mat;
						}
					}
					else
					{
						if (m_renderers[renIdx].materials.IsValidIndex(matIdx))
							m_renderers[renIdx].materials[matIdx] = mat;
					}
				}
			}
		}

		/// <summary>
		/// customMatIdx: 0 to max length (0 is the default material)
		/// targetRenIdx and targetMatIdx: -1 to max length (-1 means all)
		/// </summary>
		public virtual void ChangeToMaterial(int customMatIdx = 0, int targetRenIdx = 0, int targetMatIdx = 0)
		{
			if (m_customMaterials.IsValidIndex(customMatIdx))
			{
				ChangeMaterial(m_customMaterials[customMatIdx], targetRenIdx, targetMatIdx);
			}
		}

		public virtual void ChangeShader(Shader shader, int renIdx = 0, int matIdx = 0)
		{
			if (renIdx == -1)
			{
				for (int i = 0; i < m_renderers.Length; i++)
				{
					if (matIdx == 0)
					{
						m_renderers[i].material.shader = shader;
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[i].materials.Length; j++)
						{
							m_renderers[i].materials[j].shader = shader;
						}
					}
					else
					{
						if (m_renderers[i].materials.IsValidIndex(matIdx))
							m_renderers[i].materials[matIdx].shader = shader;
					}
				}
			}
			else
			{
				if (m_renderers.IsValidIndex(renIdx))
				{
					if (matIdx == 0)
					{
						m_renderers[renIdx].material.shader = shader;
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[renIdx].materials.Length; j++)
						{
							m_renderers[renIdx].materials[j].shader = shader;
						}
					}
					else
					{
						if (m_renderers[renIdx].materials.IsValidIndex(matIdx))
							m_renderers[renIdx].materials[matIdx].shader = shader;
					}
				}
			}
		}

		/// <summary>
		/// customShadIdx: 0 to max length (0 is the default shader)
		/// targetRenIdx and targetMatIdx: -1 to max length (-1 means all)
		/// </summary>
		public virtual void ChangeToShader(int customShadIdx = 0, int targetRenIdx = 0, int targetMatIdx = 0)
		{
			if (m_customShaders.IsValidIndex(customShadIdx))
			{
				ChangeShader(m_customShaders[customShadIdx], targetRenIdx, targetMatIdx);
			}
		}

        public virtual void SetColorProperty(MaterialColorOptions options, int renIdx = 0, int matIdx = 0)
        {
            if (options.PropertyId != 0)
                SetProperty(ShaderPropertyType.Color, options.PropertyId, options.target, renIdx, matIdx);
            else
                SetProperty(ShaderPropertyType.Color, options.propertyName, options.target, renIdx, matIdx);
        }

		public virtual void SetProperty(ShaderPropertyType propType, string propName, object propValue, int renIdx = 0, int matIdx = 0)
		{
			if (renIdx == -1)
			{
				for (int i = 0; i < m_renderers.Length; i++)
				{
					if (matIdx == 0)
					{
						SetProperty(m_renderers[i].material, propType, propName, propValue);
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[i].materials.Length; j++)
						{
							SetProperty(m_renderers[i].materials[j], propType, propName, propValue);
						}
					}
					else
					{
						if (m_renderers[i].materials.IsValidIndex(matIdx))
						{
							SetProperty(m_renderers[i].materials[matIdx], propType, propName, propValue);
						}
					}
				}
			}
			else
			{
				if (m_renderers.IsValidIndex(renIdx))
				{
					if (matIdx == 0)
					{
						SetProperty(m_renderers[renIdx].material, propType, propName, propValue);
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[renIdx].materials.Length; j++)
						{
							SetProperty(m_renderers[renIdx].materials[j], propType, propName, propValue);
						}
					}
					else
					{
						if (m_renderers[renIdx].materials.IsValidIndex(matIdx))
							SetProperty(m_renderers[renIdx].materials[matIdx], propType, propName, propValue);
					}
				}
			}
		}

		public virtual void SetProperty(ShaderPropertyType propType, int propId, object propValue, int renIdx = 0, int matIdx = 0)
		{
			if (renIdx == -1)
			{
				for (int i = 0; i < m_renderers.Length; i++)
				{
					if (matIdx == 0)
					{
						SetProperty(m_renderers[i].material, propType, propId, propValue);
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[i].materials.Length; j++)
						{
							SetProperty(m_renderers[i].materials[j], propType, propId, propValue);
						}
					}
					else
					{
						if (m_renderers[i].materials.IsValidIndex(matIdx))
						{
							SetProperty(m_renderers[i].materials[matIdx], propType, propId, propValue);
						}
					}
				}
			}
			else
			{
				if (m_renderers.IsValidIndex(renIdx))
				{
					if (matIdx == 0)
					{
						SetProperty(m_renderers[renIdx].material, propType, propId, propValue);
					}
					else if (matIdx == -1)
					{
						for (int j = 0; j < m_renderers[renIdx].materials.Length; j++)
						{
							SetProperty(m_renderers[renIdx].materials[j], propType, propId, propValue);
						}
					}
					else
					{
						if (m_renderers[renIdx].materials.IsValidIndex(matIdx))
							SetProperty(m_renderers[renIdx].materials[matIdx], propType, propId, propValue);
					}
				}
			}
		}

		protected virtual void SetProperty(Material mat, ShaderPropertyType propType, string propName, object propValue)
		{
			switch (propType)
			{
				case ShaderPropertyType.Color:
					mat.SetColor(propName, (Color)propValue);
				break;
				case ShaderPropertyType.Float:
					mat.SetFloat(propName, (float)propValue);
				break;
				case ShaderPropertyType.Int:
					mat.SetInt(propName, (int)propValue);
				break;
				case ShaderPropertyType.Texture:
					mat.SetTexture(propName, (Texture)propValue);
				break;
			}
		}

		protected virtual void SetProperty(Material mat, ShaderPropertyType propType, int propId, object propValue)
		{
			switch (propType)
			{
				case ShaderPropertyType.Color:
					mat.SetColor(propId, (Color)propValue);
				break;
				case ShaderPropertyType.Float:
					mat.SetFloat(propId, (float)propValue);
				break;
				case ShaderPropertyType.Int:
					mat.SetInt(propId, (int)propValue);
				break;
				case ShaderPropertyType.Texture:
					mat.SetTexture(propId, (Texture)propValue);
				break;
			}
		}
	}

	[System.Serializable]
	public struct MaterialColorOptions : IMaterialOptions
    {
		public string propertyName;
		[ColorUsage(true, true)]
        public Color target;

		private int _propertyId;

		public int PropertyId
        {
            get => _propertyId;
            set => _propertyId = value;
        }

        /// <summary>
        /// Set the property ID, set on Initialization
        /// </summary>
        public void SetPropertyID()
        {
            _propertyId = Shader.PropertyToID(propertyName);
        }
    }

	[System.Serializable]
	public struct MaterialFloatOptions : IMaterialOptions
    {
		public string propertyName;
        public float target;

		private int _propertyId;

		public int PropertyId
        {
            get => _propertyId;
            set => _propertyId = value;
        }
    }

	[System.Serializable]
	public struct MaterialSingleOptions : IMaterialOptions
    {
		public Material mat;
        public int renIdx;
		public int matIdx;
    }

	[System.Serializable]
	public struct MaterialSingle2Options : IMaterialOptions
    {
		public int customMatIdx;
        public int renIdx;
		public int matIdx;
    }

    public enum ShaderPropertyType
    {
        Int,
        Float,
        Color,
        Texture
    }
}