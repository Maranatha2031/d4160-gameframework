﻿namespace d4160.Systems.Render
{
    using UnityEngine;

    [System.Serializable]
    public struct ShaderProperty
    {
        public string propertyName;

        private int _propertyHash;

        public int PropertyHash => _propertyHash;

        public void CalculateHash()
        {
            if (!string.IsNullOrEmpty(propertyName))
            {
                _propertyHash = Shader.PropertyToID(propertyName);
            }
            else
            {

            }
        }
    }
}
