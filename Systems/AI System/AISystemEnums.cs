﻿namespace d4160.ECS.Component
{
    public enum TickType
    {
        /// <summary>
        /// Using Observable.Timer of UniRx plugin
        /// </summary>
        UniRx,
        /// <summary>
        /// Unity Coroutine
        /// </summary>
        Coroutine,
        /// <summary>
        /// Custom AISystem, that use Time.deltatime to calculate think period
        /// </summary>
        AISystem,
        /// <summary>
        /// Each frame on Update callback, don't recommended but need to override Update on AISystem or UniRx or coroutine
        /// </summary>
        Update,
        /// <summary>
        /// Each fixed update callback, don't recommended but need to use FixedUpdate on AISystem
        /// </summary>
        FixedUpdate
    }
}