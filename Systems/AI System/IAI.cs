namespace d4160.ECS.Component
{
    using UnityEngine.Events;

    public interface IAI
    {
        float ThinkPeriod { get; }
        float CurrentThinkTimer { get; set; }
        UnityAction OnAIThink { get; }

        void RegisterToSystem(bool register, UnityAction callback = null, float thinkPeriod = 0f);
    }
}
