namespace d4160.ECS.Component
{
    // Logic inside IAI
    public interface IAILogic
    {
        TickType TickOnType { get; }

        void SetActive(bool active, int aiLayerMask = 0);

        void Init();
    }
}
