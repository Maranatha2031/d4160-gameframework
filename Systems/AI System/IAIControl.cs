
namespace d4160.ECS.Component
{
    public interface IAIControl //: IObject
    {
        int SelectedAIScriptIdx { get; }
        void SetActiveAIComponent(bool active, int aiLayerMask = 0);
    }
}
