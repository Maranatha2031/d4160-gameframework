namespace d4160.ECS.Component
{
    /// <summary>
    /// List of AI to choice from
    /// </summary>
    /// <typeparam name="AI"></typeparam>
    public abstract class AIScriptable<AI> : ComponentScriptable<AI> where AI : IAI
    {
    }
}