namespace d4160.ECS.Component
{
    using d4160.ECS.Entity;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Component of AI that store logic for one technique like: (BT, DT, NN, ML, etc) Can use third party assets.
    /// Can be an HelperAI with Input (Player movement when run away VS Player movement indication), but it's disabled when Input is OnAction
    /// SelectionAI only Actived when Selection is > 1 selections
    /// Use the aiLayerMask but only as a Case Switch to select the correct AI logic
    /// <typeparamref name="T">Common data between AI profiles (difficulty and personality)</typeparamref>  
    /// <typeparamref name="TE">Entity to communiques from to</typeparamref>
    /// <typeparamref name="TAip">Base Class of AIProfile</typeparamref>
    /// </summary>
    public abstract class AiComponent<T, TE, TAip> //: EntityLinkComponent<T, TE>, IAI where T : IComponentData where TE : IEntity where TAip : AIProfileScriptable
    {
        [Space]
        [SerializeField]
        protected TAip m_aiProfile;

        public virtual TAip AiProfile {
            get => m_aiProfile;
            set => m_aiProfile = value;
        }

        public virtual float ThinkPeriod => 0;

        public float CurrentThinkTimer { get; set; }

        public virtual UnityAction OnAIThink => null;

        /// <summary>
        /// Use to enable or disable AI for this entity
        /// </summary>
        /// <param name="active"></param>
        /// <param name="layerMask"></param>
        public abstract void SetActive(bool active, int layerMask = 0);

        // Called by IAILogics when needs AISystem callbacks
        public virtual void RegisterToSystem(bool register, UnityAction callback = null, float thinkPeriod = 0f) { }
    }

    public enum AiTechnique
    {
        BehaviourTree,
        FiniteStateMachine,
        /// <summary>
        /// Link with Agent (ML-Agent) and Register to events that give rewards (+/-) if needed
        /// </summary>
        MachineLearning
    }
}