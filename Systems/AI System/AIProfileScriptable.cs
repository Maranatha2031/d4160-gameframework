namespace d4160.ECS.Component
{
    using d4160.Scriptables;
    using UnityEngine;

    /// <summary>
    /// Base Profile for AI, use to create profiles for entities
    /// Different in Difficulty and Personality
    /// </summary>
    public abstract class AIProfileScriptable //: ProfileScriptable, IAIProfile
    {
        [SerializeField]
        protected float m_confusion;

        public virtual float Confusion
        {
            get
            {
                return m_confusion;
            }
            set
            {
                m_confusion = value;
            }
        }
    }
}
