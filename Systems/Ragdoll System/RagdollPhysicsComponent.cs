﻿#if UNITY_PHYSICS
using UnityEngine;

namespace d4160.ECS.Component
{
    /// <summary>
    /// Simple component to control Ragdoll physics.
    /// For a full function need to disable MotionContainer and IMover components that work with Colliders and Rigidbodies
    /// Also use Trigger or Physics redirect components from to here in inner colliders and rigidbodies to allow different behaviours based on what part was hit 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class RagdollPhysicsComponent<T> //: ComponentDataWrapper<T> where T : IComponentData
    {
        [SerializeField]
        protected Rigidbody[] m_rigidbodies;
        [SerializeField]
        protected Collider[] m_colliders;

        protected void SetActiveInternal(bool active, int needPartsLayerMask = 0)
        {
            for (int i = 0; i < m_rigidbodies.Length; i++)
            {
                m_rigidbodies[i].isKinematic = !active;
                m_colliders[i].enabled = active;
            }
        }
    }
}
#endif