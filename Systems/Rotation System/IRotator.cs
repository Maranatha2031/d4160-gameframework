﻿namespace d4160.Systems.Rotation
{
    using UnityEngine;

    public interface IRotator
    {
        void Rotate(Vector3 axisEuler);
    }
}