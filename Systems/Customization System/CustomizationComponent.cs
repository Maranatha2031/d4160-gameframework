namespace d4160.ECS.Entity
{
    /// <summary>
    /// To store customizations (only sensorial customization) like meshes, textures, etc. that is part of other entities
    /// Use the same Manager of the Entity that was construct for 
    /// </summary>
    /// <typeparam name="EData"></typeparam>
    /// <typeparam name="ETypeData"></typeparam>
    public abstract class CustomizationComponent<EData, ETypeData>// : EntityComponent<EData, ETypeData>, ICustom where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData
    {
    }
}
