namespace d4160.Scriptables
{
    // For Scriptables (don't a entire Database) that can be intercanbiable, so has a lot with the same Entities on it (ids) but different appareance (e.g. Another set of CubeTerrains)
    public interface ICustomizablePackage
    {
        string PackageName { get; }
    }
}