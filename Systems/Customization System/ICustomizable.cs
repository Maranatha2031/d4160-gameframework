namespace d4160
{
	/// <summary>
	/// Send the Scriptable the entity data and try to get custom values for Renderer and Audio Components
	/// For Entities and Scriptables (Database) that have intercambiable parts
	/// </summary>
	public interface ICustomizable
	{

	}
}
