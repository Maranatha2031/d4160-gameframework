namespace d4160.ECS.Entity
{
    using UnityEngine;

    /// <summary>
    /// To store sensitive customization like visual and auditive (for entities)
    /// In the form of (textures, meshes, sprites, avatarmask, audioclips),etc
    /// For changes of complete entities, use a CustomizationPart Components
    /// </summary>
    public abstract class CustomizationScriptable<C> //: EntityScriptable<C> where C : ICustom
    {
    }
}
