namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// Interface for the DifficultySet component
    /// </summary>
    public interface IDifficultySet//: IComponent
    {
        Vector2Int LevelLimits { get; }

        int CurrentLevel { get; }

        IDifficultyOptions CurrentDifficultyOptions { get; }
    }

    /// <summary>
    /// Options for the current level of difficulty
    /// </summary>
    public interface IDifficultyOptions //: IOptions
    {
        
    }
    
    /// <summary>
    /// One Phase of difficulty to store data when Animation Curve can't be used
    /// </summary>
    public interface IDifficultyPhase //: IObject
    {
        
    }
}