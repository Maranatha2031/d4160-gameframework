namespace d4160.ECS.Component
{
    using d4160.Scriptables;
    /// <summary>
    /// To store traits and difficulty values
    /// As DefficultyLevel (Hard, Easy, Normal)
    //[CreateAssetMenu(fileName = "DifficultyProfile", menuName = "Scriptable/Profile/Difficulty/___")]
    /// </summary>
    public abstract class DifficultySetProfileScriptable //: ProfileScriptable
    {

    }
}