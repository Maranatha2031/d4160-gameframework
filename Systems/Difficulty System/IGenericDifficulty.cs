﻿namespace d4160.Systems.Difficulty
{
    public interface IGenericDifficulty<T>
    {
        T CurrentDifficulty { get; }
    }
}
