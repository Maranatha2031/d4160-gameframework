﻿namespace d4160.Systems.Difficulty
{
    using UnityEngine;

    public abstract class DifficultyBase : ScriptableObject
    {
        public abstract void CalculateDifficulty(int difficultyLevel);
    }
}
