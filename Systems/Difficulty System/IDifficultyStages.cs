﻿namespace d4160.Systems.Difficulty
{
    /// <summary>
    /// When an object (no Set) is subdivided in difficulty stages like Easy / Medium / Hard
    /// </summary>
    public interface IDifficultyStages
    {
        int DifficultyStage { get; set; }
    }
}
