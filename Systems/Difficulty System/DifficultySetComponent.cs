namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// Component to store a set of difficulties
    /// Generally used by Games and MultipurposeMachines
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class DifficultySetComponent<T> //: ComponentDataWrapper<T>, IDifficultySet where T : IComponentData
    {
        protected int m_currenLevel = 0;
        protected Vector2Int m_levelLimits = Vector2Int.zero;

        public virtual Vector2Int LevelLimits { 
            get {
                return m_levelLimits;
            }
            set {
                m_levelLimits = value;
            }
        }

        public virtual int CurrentLevel {
            get {
                return m_currenLevel;
            }
            set {
                m_currenLevel = value;
                if (value < m_levelLimits.x)
                    m_currenLevel = m_levelLimits.x;
                else if (value > m_levelLimits.y)
                    m_currenLevel = m_levelLimits.y;
            }
        }

        /// <summary>
        /// The current difficulty options
        /// Can be only one part (by # level | IDifficultyOptions) or all the scriptable 
        /// </summary>
        public abstract IDifficultyOptions CurrentDifficultyOptions
        {
            get;
        }
        // Traits
        // DifficultyProfiles

        /// <summary>
        /// Try to set the default level limits, since don't accept any parameter is used with serialized values
        /// </summary>
        public virtual void SetDefaultLevelLimits()
        {
        }
    }
}