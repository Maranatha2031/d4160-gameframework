namespace d4160.Systems.Construction
{
    public interface IConstructable
    {
        void Construct();
    }
}