﻿namespace d4160.Systems.Common
{
    public enum UnityInitMethod : byte 
    {
        Awake,
        OnEnable,
        Start
    }

    public enum UnityInitMethodWithManual : byte
    {
        Awake,
        OnEnable,
        Start,
        Manual
    }

    public enum UnityLoopMethod : byte
    {
        FixedUpdate,
        Update,
        LateUpdate
    }

    public enum UnityLoopMethodWithManual : byte
    {
        FixedUpdate,
        Update,
        LateUpdate,
        Manual
    }
}
