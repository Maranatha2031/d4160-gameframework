﻿namespace d4160.Systems.Common
{
    using UnityEngine;

    public interface IFirstTransform
    {
        Transform FTransform { get; }
    }

    public interface ISecondTransform
    {
        Transform STransform { get; }
    }

    public interface IThirdTransform
    {
        Transform Transform { get; }
    }
}
