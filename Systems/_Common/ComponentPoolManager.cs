﻿namespace d4160.ECS.Component
{
    using d4160.ECS.Entity;
    using UnityEngine;
    using d4160.Core;

    /// <summary>
    /// 
    /// </summary>
    public abstract class ComponentPoolManager<S> : ComponentManager<S> where S : MonoBehaviour
    {
        /*[SerializeField]
        protected MonoBehaviorPoolData m_poolData;

        public virtual MonoBehaviorPoolData PoolData
        {
            get
            {
                return m_poolData;
            }
        }*/

        protected override void Awake()
        {
            base.Awake();

            FillObjectPool();
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual void FillObjectPool()
        {
            //m_poolData.FillObjectPool();
        }

        public virtual T PopObjectFromPool<T>() where T : MonoBehaviour
        {
            return PopObjectFromPool() as T;
        }

        public virtual MonoBehaviour PopObjectFromPool()
        {
            return null;
            /*MonoBehaviour go;
            if (m_poolData.PopObject(out go))
                return go;
            else
                return m_poolData.InstanceLackingObject();*/
        }

        public virtual void PushObjectToPool(MonoBehaviour go)
        {
            //m_poolData.PushObject(go);
        }
    }
}