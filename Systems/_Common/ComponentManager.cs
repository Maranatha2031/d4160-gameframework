namespace d4160.ECS.Component
{
    using UnityEngine;
    using d4160.Core;

    public abstract class ComponentManager<T> : Singleton<T> where T : MonoBehaviour
    {
    }
}
