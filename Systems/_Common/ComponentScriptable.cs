namespace d4160.ECS.Component
{
    using UnityEngine;
    using d4160.Core;
#if UNITY_EDITOR && ODIN_INSPECTOR
    using Sirenix.OdinInspector;
#endif

    /// <summary>
    /// For components like AI, Input or Motions that need a list of component to choice for
    /// This is part of the EntityScriptable as Localization and Customization
    /// </summary>
    /// <typeparam name="D"></typeparam>
    public abstract class ComponentScriptable<D> : ScriptableObject //where D : IComponent
    {
        [SerializeField]
#if UNITY_EDITOR && ODIN_INSPECTOR
        [OnValueChanged("FixDIds", true)]
#endif
        protected D[] m_components;

        public virtual D[] Components
        {
            get
            {
                return m_components;
            }
        }

        public virtual D GetEntityAt(int idx)
        {
            if (m_components.IsValidIndex(idx))
                return m_components[idx];

            return default;
        }

        public virtual D GetEntityWith(int id)
        {
            /*for (int i = 0; i < m_components.Length; i++)
            {
                if (m_components[i].Id == id)
                    return m_components[i];
            }*/

            return default;
        }

        private void FixDIds()
        {
            for (int i = 0; i < m_components.Length; i++)
            {
                if (m_components[i] != null)
                {
                    //m_components[i].Id = i + 1;
                }
            }
        }
    }
}
