﻿namespace d4160.Systems.Common
{
    using UnityEngine;

    [System.Serializable]
    public struct Vector3Bool
    {
        public bool x;
        public bool y;
        public bool z;
    }

    [System.Serializable]
    public struct Vector2Bool
    {
        public bool x;
        public bool y;
    }
}
