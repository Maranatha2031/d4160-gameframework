﻿namespace d4160.Systems.Common
{
    public interface IResetable
    {
        void Reset();
    }
}
