﻿namespace d4160.Systems.Scale
{
    using UnityEngine;

    public interface IScaler
    {
        void Scale(Vector3 localScale);
    }
}