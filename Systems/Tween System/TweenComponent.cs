namespace d4160.ECS.Component
{
    using System;
    using UnityEngine;

    /// <summary>
    /// Generic class for Tween or Tween Sequences part of a TweenSet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TS"></typeparam>
    public abstract class TweenComponent<T, TS> : MonoBehaviour//: ComponentDataWrapper<T> where T : IComponentData where TS : ITweenSet 
    {
        protected TS m_tweenSet;

        public TS TweenSet {
            get {
                if (m_tweenSet == null)
                    CatchComponents();

                return m_tweenSet;
            }
        }

        protected void CatchComponents()
        {
            if(m_tweenSet == null)
                m_tweenSet = GetComponentInParent<TS>();

            if (m_tweenSet == null)
                m_tweenSet = transform.parent.GetComponentInParent<TS>();
        }

        /// <summary>
        /// Instead of tweenIdx can use a enum to convert from a List Enum of Tweens belongs to this Entity
        /// Generally for two uses Forward|Backward or like Speed 1, -1
        /// </sumary>
        public abstract void Play(int tweenIdx = 0, Action startCallback = null, Action completeCallback = null);

        public virtual void Stop(int tweenIdx = 0, Action stopCallback = null) { }

        protected virtual void DoTweenMaterial(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        protected virtual void DoTweenTranslate(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        protected virtual void DoTweenScale(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}
        
        protected virtual void DoTweenRotate(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        /// <summary>
        /// Automatically Scale the related object with 'opIdx' with 'tranformOptions', so used to prepare Tweens as predeterminated values before play (the tween)
        /// </summary>
        /// <param name="opIdx"></param>
        /// <param name="options"></param>
        //protected virtual void Scale(int opIdx = 0, ITransformOptions options = null){}
        protected virtual void ChangeMaterial(int opIdx = 0, IMaterialOptions options = null){}
        /// <summary>
        /// Automatically Set a material property of a related object with 'opIdx' with IMaterialOptions
        /// </summary>
        /// <param name="opIdx"></param>
        /// <param name="options"></param>
        protected virtual void SetMaterialProperty(int opIdx = 0, IMaterialOptions options = null){}

        public virtual void ForceStopAll(){}
    }
}
