namespace d4160.ECS.Component
{
    using d4160.ECS.Entity;
    using System;

    /// <summary>
    /// For a set of tweens for a single entity
    /// Recommendation: Use Single classes for each Tweens sequences for a best arrange of the code having a array of some implementation of Tween<T, TS> and make more tweens from there (this way allow reutilization of code)
    /// Can use any third party plugin
    /// </summary>
    public abstract class TweenSetComponent<T, E> //: EntityLinkComponent<T, E>, ITweenSet where T : IComponentData where E : IEntity
    {
        /// <summary>
        /// Instead of tweenIdx can use a enum to convert from a List Enum of Tweens belongs to this Entity
        /// </sumary>
        public abstract void Play(int tweenIdx = 0, Action startCallback = null, Action completeCallback = null);

        public virtual void Stop(int tweenIdx = 0, Action stopCallback = null) { }

        protected virtual void DoMaterialTween(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        protected virtual void DoTranslateTween(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        protected virtual void DoScaleTween(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}
        
        protected virtual void DoRotateTween(int opIdx = 0, ITweenOptions options = null, Action startCallback = null, Action completeCallback = null){}

        /// <summary>
        /// Automatically Scale the related object with 'opIdx' with 'tranformOptions', so used to prepare Tweens as predeterminated values before play (the tween)
        /// </summary>
        /// <param name="opIdx"></param>
        /// <param name="options"></param>
        //protected virtual void Scale(int opIdx = 0, ITransformOptions options = null){}
        protected virtual void ChangeMaterial(int opIdx = 0, IMaterialOptions options = null){}
        /// <summary>
        /// Automatically Set a material property of a related object with 'opIdx' with IMaterialOptions
        /// </summary>
        /// <param name="opIdx"></param>
        /// <param name="options"></param>
        protected virtual void SetMaterialProperty(int opIdx = 0, IMaterialOptions options = null){}

        public virtual void ForceStopAll(){}
    }
}
