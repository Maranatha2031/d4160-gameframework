namespace d4160.ECS.Component
{
    using System;

    /// <summary>
    /// When its necesary that components call tweens, for Entities use a custom TweenSet
    /// </summary>
 	public interface ITweenSender //: IObject
    {
		void DoTween(int tweenIdx = 0, ITweenOptions options= null, Action startCallback = null, Action completeCallback = null);
	}

    public interface ITweenOptions //: IOptions
    {

    }
}