namespace d4160.ECS.Component
{
    public abstract class MotionScriptable<M> : ComponentScriptable<M> where M : IAnimation
    {
    }
}