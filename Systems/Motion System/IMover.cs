namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// For components that allow movements like Physics, Transform and NavMeshAgent for 3D
    /// </summary>
    public interface IMover //: IComponent
    {
        Vector3 Velocity { get; set; }

        void SetDestination(Vector3 targetPoint, int idx = 0);

        void SetDestinationTarget(Transform target);

        void SetVelocity(float vel, Vector3 axis);

        void Stop();
    }

    public interface IMover2D// : IComponent
    {
        Vector2 Velocity { get; set; }

        void SetDestination(Vector2 targetPoint, int idx = 0);

        void SetDestinationTarget(Transform target);

        void SetVelocity(float vel, Vector2 axis);

        void Stop();
    }
}