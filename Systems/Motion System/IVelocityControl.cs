﻿namespace d4160.Systems.Motion
{
    using UnityEngine;

    public interface IVelocityControl
    {
        Vector3 Velocity { get; set; }
    }
}