﻿namespace d4160.Systems.Motion
{
    using UnityEngine;

    public interface IMoveBetweenTransforms
    {
        void MoveBetween(Transform origin, Transform end);
    }

    public interface IMoveBetweenVectors3
    {
        void MoveBetween(Vector3 origin, Vector3 end);
    }

    public interface IMoveBetweenVectors2
    {
        void MoveBetween(Vector2 origin, Vector2 end);
    }
}
