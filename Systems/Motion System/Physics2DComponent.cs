#if UNITY_PHYSICS2D
namespace d4160.ECS.Component
{
    using UnityEngine;
    using d4160.Extensions;

    /// <summary>
    /// Component which have util methods to use for control a character with Rigidbody
    /// Need a Rigidbody and a Collider in the parent or in this GameObject
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Physics2DComponent<T> //: ComponentDataWrapper<T>, IMover2D, IPhysics2D where T : IComponentData
    {
        protected Rigidbody2D m_rigidbody2D;
        protected Collider2D m_collider2D;

        public virtual Rigidbody2D Rigidbody2D {
            get {
                if (m_rigidbody2D)
                    return m_rigidbody2D;

                /*if (!m_rigidbody2D)
                    m_rigidbody2D = GetComponent<Rigidbody2D>();
                if (!m_rigidbody2D)
                    m_rigidbody2D = GetComponentInParent<Rigidbody2D>();*/

                return m_rigidbody2D;
            }
        }

        public virtual Collider2D Collider2D {
            get {
                if (m_collider2D)
                    return m_collider2D;

                //if (!m_collider2D)
                //    m_collider2D = this.GetComponent<Collider2D>(true, false, true);

                return m_collider2D;
            }
        }
        
        public virtual Vector2 Velocity
        {
            get => m_rigidbody2D.velocity;
            set => m_rigidbody2D.velocity = value;
        }

        protected void CatchComponents()
        {
            /*if (!m_rigidbody2D)
                m_rigidbody2D = this.GetComponent<Rigidbody2D>(true, false, true);

            if (!m_collider2D)
                m_collider2D = this.GetComponent<Collider2D>(true, false, true);*/
        }

        protected void SetActiveInternal(bool active, int neededPartsLayerMask = 0)
        {
            if (!m_rigidbody2D || !m_collider2D)
                CatchComponents();

            m_rigidbody2D.simulated = active;
            m_collider2D.enabled = active;
        }

        public virtual void SetDestination(Vector2 targetPoint, int idx = 0)
        {
            m_rigidbody2D.MovePosition(targetPoint);
        }

        public virtual void SetDestinationTarget(Transform target)
        {}

        public virtual void SetVelocity(float vel, Vector2 axis)
        {
            Vector2 newVel = m_rigidbody2D.velocity;

            if (axis == Vector2.right)
                newVel.x = vel;
            else if (axis == Vector2.up)
                newVel.y = vel;

            m_rigidbody2D.velocity = newVel;
        }

        /* TO DO: Add options, like return rotation to identity, don't restore angular velocity, etc. */
        public virtual void Stop()
        {
            m_rigidbody2D.velocity = Vector2.zero;
            m_rigidbody2D.angularVelocity = 0f;
        }
    }
}
#endif