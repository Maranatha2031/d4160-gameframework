﻿namespace d4160.Systems.Motion
{
    public interface IAutoMoveTo
    {
        void MoveTo();
    }
}