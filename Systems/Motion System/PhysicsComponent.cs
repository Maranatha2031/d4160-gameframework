#if UNITY_PHYSICS
using d4160.Extensions;
using UnityEngine;

namespace d4160.ECS.Component
{
    /// <summary>
    /// Component which have util methods to use for control a character with Rigidbody
    /// Need a Rigidbody and a Collider in the root transform or in its first children
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class PhysicsComponent<T> : MonoBehaviour// : ComponentDataWrapper<T>, IPhysics3D, IMover where T : IComponentData
    {
        [SerializeField] protected Collider[] m_colliders;
        [SerializeField] protected PhysicMaterial[] m_customPhysicMaterials;

        protected Rigidbody m_rigidbody;
        protected bool m_isKinematicFirstState = false;

        public virtual Rigidbody Rigidbody {
            get {
                if (!m_rigidbody)
                    m_rigidbody = this.GetComponent<Rigidbody>(true, false, true);

                return m_rigidbody;
            }
        }
        public virtual Collider[] Colliders => null;

        public virtual Vector3 Velocity
        {
            get => m_rigidbody.velocity;
            set => m_rigidbody.velocity = value;
        }

        protected void CatchComponents()
        {
            if (!m_rigidbody)
                m_rigidbody = this.GetComponent<Rigidbody>(true, false, true);
        }

        public void Initialize()
        {
            m_isKinematicFirstState = Rigidbody.isKinematic;
        }

        protected void SetActiveInternal(bool active, int neededPartsLayerMask = 0)
        {
            if (!active)
            {
                m_rigidbody.isKinematic = true;
            }
            else
            {
                m_rigidbody.isKinematic = m_isKinematicFirstState;
            }

            for (int i = 0; i < m_colliders.Length; i++)
            {
                m_colliders[i].enabled = active;
            }
        }

        public virtual void SetDestination(Vector3 targetPoint, int idx = 0)
        {
            m_rigidbody.MovePosition(targetPoint);
        }

        public virtual void SetDestinationTarget(Transform target)
        {
        }

        public virtual void SetVelocity(float vel, Vector3 axis)
        {
            Vector3 newVel = m_rigidbody.velocity;

            if (axis == Vector3.right)
                newVel.x = vel;
            else if (axis == Vector3.up)
                newVel.y = vel;
            else if (axis == Vector3.forward)
                newVel.z = vel;

            m_rigidbody.velocity = newVel;
        }

        public virtual void Stop()
        {
            m_rigidbody.velocity = Vector3.zero;
        }

        public virtual void MovePosition(Vector3 pos)
        { }

        public virtual void MoveRotation(Quaternion rot)
        { }
    }
}
#endif