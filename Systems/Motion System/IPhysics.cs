namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// For Physics Components: 2D and 3D common interface
    /// </summary>
    public interface IPhysics// : IComponent
    {
        
    }

#if UNITY_PHYSICS
    public interface IPhysics3D : IPhysics
    {
        Vector3 Velocity { get; set; }

        Rigidbody Rigidbody { get; }

        Collider[] Colliders { get; }

        void SetVelocity(float vel, Vector3 axis);

        void MovePosition(Vector3 pos);

        void MoveRotation(Quaternion rot);
    }
#endif

#if UNITY_PHYSICS2D
    public interface IPhysics2D : IPhysics
    {
        Vector2 Velocity { get; set; }

        Rigidbody2D Rigidbody2D { get; }

        Collider2D Collider2D { get; }

        void SetVelocity(float vel, Vector2 axis);
    }
#endif
}