#if UNITY_AI
using UnityEngine;
using UnityEngine.AI;

namespace d4160.ECS.Component
{
    /// Allow third party link (creating a class), need a EntityLink to Player to call Motions (with name or id)
    /// For a NavMeshAgent for 2D need another IMover to do this (for example PolyNav plugin on AssetStore)
    public abstract class NavMeshAgentComponent<T> : ComponentDataWrapper<T>, IPhysics where T : IComponentData, IMover
    {
        protected NavMeshAgent m_agent;

        public virtual NavMeshAgent Agent {
            get {
                if (m_agent)
                    return m_agent;

                CatchComponents();
                
                return m_agent;
            }
        }

        public virtual Vector3 Velocity {
            get {
                return m_agent.velocity;
            }
            set {
                m_agent.velocity = value;
            }
        }

        protected override void CatchComponents()
        {
            if (!m_agent)
                m_agent = GetComponent<NavMeshAgent>();
            if (!m_agent)
                m_agent = GetComponentInParent<NavMeshAgent>();
        }
        
        public virtual void SetDestination(Vector3 targetPoint)
        {
            m_agent.SetDestination(targetPoint);
        }

        public virtual void SetDestinationTarget(Transform target)
        {
        }

        public virtual void SetVelocity(float vel, Vector2 axis)
        {
        }

        public virtual void SetVelocity(float vel, Vector3 axis)
        {
            Vector3 newVel = m_agent.velocity;

            if (axis == Vector3.right)
                newVel.x = vel;
            else if (axis == Vector3.up)
                newVel.y = vel;
            else if (axis == Vector3.forward)
                newVel.z = vel;

            m_agent.velocity = newVel;
        }

        public virtual void Stop()
        {
            m_agent.isStopped = true;
        }

        public virtual void Resume()
        {
            m_agent.isStopped = false;
        }
    }
}
#endif