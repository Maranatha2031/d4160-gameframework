﻿namespace d4160.ECS.Component
{
    using UnityEngine;

    /// Allow third party link (creating a class), need a EntityLink to Player to call Motions (with name or id)
    public abstract class TransformComponent<T> //: ComponentDataWrapper<T>, IMover where T : IComponentData
    {
        [SerializeField] protected Transform m_rootTransform;
        protected Vector3 m_velocity;

        public virtual Vector3 Velocity {
            get {
                return m_velocity;
            }
            set {
               m_velocity = value;
            }
        }

        protected void CatchComponents()
        {
            //if (!m_rootTransform)
            //    m_rootTransform = transform;
        }

        // idx: To inject some configurations as layermask
        // To inject more complex values need some IEntityOptions that set values on properties sended here
        public virtual void SetDestination(Vector3 targetPoint, int idx = 0)
        {
        }

        public virtual void SetDestinationTarget(Transform target)
        {
        }

        public virtual void SetVelocity(float vel, Vector2 axis)
        {
        }

        public virtual void SetVelocity(float vel, Vector3 axis)
        {
            Vector3 newVel = m_velocity;

            if (axis == Vector3.right)
                newVel.x = vel;
            else if (axis == Vector3.up)
                newVel.y = vel;
            else if (axis == Vector3.forward)
                newVel.z = vel;

            m_velocity = newVel;
        }

        public virtual void Stop()
        {
            m_velocity = Vector3.zero;
        }

        public virtual void LookAt(Transform target) { }
    }

    [System.Serializable]
    public struct TransformVector3Options// : ITransformOptions
    {
        public Vector3 target;

        public TransformVector3Options(Vector3 target)
        {
            this.target = target;
        }
    }
}