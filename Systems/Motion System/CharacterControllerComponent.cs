#if UNITY_PHYSICS
using UnityEngine;

namespace d4160.ECS.Component
{
    /// Allow third party link (creating a class), need a EntityLink to Player to call Motions (with name or id)
    /// For a NavMeshAgent for 2D need another IMover to do this (for example PolyNav plugin on AssetStore)
    public abstract class CharacterControllerComponent<T> : MonoBehaviour//: ComponentDataWrapper<T>, IMover where T : IComponentData
    {
        protected CharacterController m_controller;

        public virtual CharacterController CharacterController {
            get {
                if (m_controller)
                    return m_controller;

                CatchComponents();
                
                return m_controller;
            }
        }

        public virtual Vector3 Velocity {
            get {
                return m_controller.velocity;
            }
            set {
                
            }
        }

        protected void CatchComponents()
        {
            if (!m_controller)
                m_controller = GetComponent<CharacterController>();
            if (!m_controller)
                m_controller = GetComponentInParent<CharacterController>();
        }
        
        public virtual void SetDestination(Vector3 targetPoint, int idx = 0)
        {
        }

        public virtual void SetDestinationTarget(Transform target)
        {
        }

        public virtual void SetVelocity(float vel, Vector2 axis)
        {
        }

        public virtual void SetVelocity(float vel, Vector3 axis)
        {
            m_controller.SimpleMove(vel * axis);
        }

        public virtual void Stop()
        {
            
        }
    }
}
#endif