namespace d4160.ECS.Component
{
    using UnityEngine;

    /// Allow third party link (creating a class), need a EntityLink to Player to call Motions (with name or id)
    /// For only one main parent particle (can have sub emitters)
    public abstract class ParticleMoverComponent<T> //: ComponentDataWrapper<T>, IMover where T : IComponentData
    {
        // Can be a list (like Ragdoll)
        protected ParticleSystem m_particleSystem;
        // Can also be a list
        protected ParticleSystem.Particle[] m_particles;

        public virtual ParticleSystem ParticleSystem {
            get {
                if (m_particleSystem)
                    return m_particleSystem;

                // Since all UnityCallbacks is called by Object, we need to check before send 
                /*if (!m_particleSystem)
                    m_particleSystem = GetComponent<ParticleSystem>();
                if (!m_particleSystem)
                    m_particleSystem = GetComponentInParent<ParticleSystem>();
                    */
                return m_particleSystem;
            }
        }

        public virtual Vector3 Velocity {
            get {
                return m_particles[0].velocity;
            }
            set {
                m_particles[0].velocity = value;

                m_particleSystem.SetParticles(m_particles, 1);
            }
        }


        protected void CatchComponents()
        {
            /*if (!m_particleSystem)
                m_particleSystem = GetComponent<ParticleSystem>();
            if (!m_particleSystem)
                m_particleSystem = GetComponentInParent<ParticleSystem>();*/

            int particlesCount = m_particleSystem.GetParticles(m_particles);
        }

        protected void SetActiveInternal(bool active, int needPartsLayerMask = 0)
        {
            // Like Unregister to the GlobalPhysics engine
            // Can be this form, but also on any process can evaluate IsActive flag
            var emiModule = m_particleSystem.emission;
            emiModule.enabled = active;
        }

        public virtual void SetDestination(Vector3 targetPoint, int idx = 0)
        {
        }

        public virtual void SetDestinationTarget(Transform target)
        {
        }

        public virtual void SetVelocity(float vel, Vector2 axis)
        {
        }

        public virtual void SetVelocity(float vel, Vector3 axis)
        {
            Vector3 newVel = m_particles[0].velocity;

            if (axis == Vector3.right)
                newVel.x = vel;
            else if (axis == Vector3.up)
                newVel.y = vel;
            else if (axis == Vector3.forward)
                newVel.z = vel;

            m_particles[0].velocity = newVel;

            m_particleSystem.SetParticles(m_particles, 1);
        }

        public virtual void Stop()
        {
            m_particles[0].velocity = Vector3.zero;

            m_particleSystem.SetParticles(m_particles, 1);
        }
    }
}