﻿namespace d4160.Systems.Motion
{
    using UnityEngine;

    public interface IMoveTowards
    {
        void MoveTowards(Vector3 direction);
    }
}