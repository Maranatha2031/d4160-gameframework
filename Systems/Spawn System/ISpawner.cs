namespace d4160.ECS.Component
{
    public interface ISpawner //: IComponent
    {
        void StartTimedSpawn(int idx = 0);

        void Spawn(int idx = 0);
    }
}