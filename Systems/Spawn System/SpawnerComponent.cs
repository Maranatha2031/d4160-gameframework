namespace d4160.ECS.Component
{
    using d4160.ECS.Entity;

    public abstract class SpawnerComponent<T> //: ComponentDataWrapper<T>, ISpawner where T : IComponentData
    {
        // Helper idx as case selector
        public abstract void StartTimedSpawn(int idx);

        public abstract void Spawn(int idx = 0);

        //protected abstract IEntity GetSpawnObject(int idx = 0);

        /*protected virtual void SpawnInternal(int idx = 0)
        {

        }*/
    }
}