﻿namespace d4160
{
	public interface IGrabbable : IInteractable
	{
		void Grab(IGrabber grabber);

		void Ungrab(IGrabber grabber);

        void ForceUngrab();
    } 
}
