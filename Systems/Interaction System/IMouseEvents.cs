namespace d4160
{
    public interface IMouseEvents
    {
    }

    public interface IMouseEventsFull : IMouseEvents
    {
        void OnMouseEnter();
        void OnMouseExit();
    }

    public interface IMouseEventsSingle1 : IMouseEvents
    {
        void OnMouseEnter();
    }

    public interface IMouseEventsSingle2 : IMouseEvents
    {
        void OnMouseExit();
    }

    public interface IMouseEventsBasic1 : IMouseEvents
    {
        void OnMouseEnter();
        void OnMouseExit();
    }
}