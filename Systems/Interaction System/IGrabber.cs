namespace d4160
{
	public interface IGrabber : IInteractor
	{
		void Grab(IGrabbable grabbable);

		void Ungrab(IGrabbable grabbable);
	}
}