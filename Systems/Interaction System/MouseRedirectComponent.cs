namespace d4160.ECS.Component
{
    using UnityEngine;

    public abstract class MouseRedirectComponent<T, IMEvents> //: ComponentDataWrapper<T> where T : IComponentData  where IMEvents : IMouseEvents
	{
        [SerializeField]
		[Tooltip("Is this is null, try to find Interface on parent GO")]
		protected GameObject entityTargetGO;
		protected IMEvents m_eventsReceiver;

		protected void CatchComponents()
		{
			//if (!entityTargetGO)
			//	m_eventsReceiver = GetComponentInParent<IMEvents>();
			//else
			//	m_eventsReceiver = entityTargetGO.GetComponent<IMEvents>();
		}
	}

}