namespace d4160
{
	public interface IToucher : IInteractor
	{
		void Touch(ITouchable touchable);

		void Untouch(ITouchable touchable);
	}
}