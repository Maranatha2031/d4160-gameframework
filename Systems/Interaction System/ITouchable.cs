namespace d4160
{
	public interface ITouchable : IInteractable
	{
		void Touch(IToucher toucher);

		void Untouch(IToucher toucher);
	}
}