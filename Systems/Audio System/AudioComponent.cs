namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// Ensure one per Entity
    /// Can have a list of AudioSources and ReberbZones (THIS NEED TO BE IN AUDIOMANAGER)
    /// Generally AudioSources have AudioMixers (to communicates with Master) (This need to be in AUDIO MANAGER)
    /// Work with AudioProfiles (struct) ON AUDIO MANAGER
    /// This call for AudioSource to the AUDIO MANAGER (using name or id) and this set to some ChildObject, 
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class AudioComponent<T> : MonoBehaviour//: ComponentDataWrapper<T>, IAudio where T : IComponentData, IAudioData
    {
        [SerializeField]
        protected PlayAtStart m_playAtStart;

        protected virtual Transform PlayPosition
        {
            get
            {
                return this.transform;
            }
        }

        protected virtual void Start()
        {
            CheckPlayAtStart();
        }

        /// <summary>
        /// Play using a point
        /// </summary>
        /// <param name="idx">The idx of the Audio, use idx to -1 to choice random</param>
        public virtual void PlayAt(Vector3 point, int idx = 0)
        {
            //m_serializedData.PlayAt(point, idx);
        }

        /// <summary>
        /// Play using the Manager.transform
        /// </summary>
        /// <param name="idx">The idx of the Audio, use idx to -1 to choice random</param>
        public virtual void Play(int idx = 0)
        {
            //m_serializedData.Play(idx);
        }

        /// <summary>
        /// Play using this.transform
        /// </summary>
        /// <param name="idx">The idx of the Audio, use idx to -1 to choice random</param>
        public virtual void PlayAt(int idx = 0)
        {
            PlayAt(transform.position, idx);
        }

        public virtual void PlayAt(Transform parent, int idx = 0)
        {
            //m_serializedData.PlayAt(parent, idx);
        }

        protected virtual void CheckPlayAtStart()
        {
            if (!m_playAtStart.m_playAtStart)
                return;

            switch (m_playAtStart.m_positionType)
            {
                case AudioPlayPositionType.This:
                    PlayAt(m_playAtStart.m_playIndex);
                    break;
                case AudioPlayPositionType.Manager:
                    Play(m_playAtStart.m_playIndex);
                    break;
                case AudioPlayPositionType.CustomPosition:
                    PlayAt(PlayPosition.position, m_playAtStart.m_playIndex);
                    break;
                case AudioPlayPositionType.CustomParent:
                    PlayAt(PlayPosition, m_playAtStart.m_playIndex);
                    break;
                default:
                    break;
            }
        } 
    }

    [System.Serializable]
    public struct PlayAtStart
    {
        public bool m_playAtStart;
        [Tooltip("Index of the custom")]
        public int m_playIndex;
        public AudioPlayPositionType m_positionType;
    }

    public enum AudioPlayPositionType
    {
        This,
        Manager,
        CustomPosition,
        CustomParent
    }
}