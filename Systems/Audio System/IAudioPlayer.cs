namespace d4160.ECS.Component
{
    /*public enum AudioEnum
    {
    }*/

    /// <summary>
    /// Can call Audios according the enum reemplazable index, generally is a defined enum of audios, the target Vector3 or parent Transform need to be set before as property
    /// </summary>
    public interface IAudioPlayer //: IObject
    {
        void PlayAudio(int audioIdx = 0);
    }
}