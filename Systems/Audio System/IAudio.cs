namespace d4160.ECS.Component
{
    using UnityEngine;

    public interface IAudio : IAudioData//, IComponent
    {
        
    }

    public interface IAudioData
    {
        void PlayAt(Vector3 point, int idx = 0);

        void Play(int idx = 0);

        void PlayAt(Transform parent, int idx = 0);
    }
}
