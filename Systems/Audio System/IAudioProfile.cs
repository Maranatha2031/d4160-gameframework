namespace d4160.ECS.Component
{
    using d4160.Scriptables;
    using System;
    using UnityEngine;

    /// <summary>
    /// General needed data, but according to the use don't need all
    /// Some data can get from AudioSource
    /// </summary>
    public interface IAudioProfile// : IProfile
    {
        AudioDimension AudioDimension { get; }

        AudioClip Clip { get; }

        float Pitch { get; }

        AudioType AudioType { get; }

        bool Loop { get; }

        float Volume { get; }

        float SpatialBlend { get; }

        AnimationCurve ReverbZoneMix { get; }

        float Spread { get; }

        Vector2 FalloffDistance { get; }

        AudioRolloffMode FalloffCurve { get; }

        AnimationCurve VolumeFalloffCurve { get; }

        float Delay { get; }

        int Play(float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null);

        int PlayAt(Vector3 point, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null);

        int PlayAt(Transform parent, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null);

        bool Stop();

#if OCULUS
        OSPProps OSPProps { get; }

        bool UseVirtualSpeaker { get; }
#endif
    }
}
