﻿namespace d4160.ECS.Component
{
    using System;
    using UnityEngine;

    [Serializable]
    public class AudioProfile_Default : IAudioProfile
    {
        public AudioProfile_Default()
        {
            playback = SoundFXNext.Random;
            volume = 1.0f;
            pitchVariance = Vector2.one;
            falloffDistance = new Vector2(1.0f, 25.0f);
            falloffCurve = AudioRolloffMode.Linear;
            volumeFalloffCurve = new AnimationCurve(new Keyframe[2] { new Keyframe(0f, 1.0f), new Keyframe(1f, 1f) });
            reverbZoneMix = new AnimationCurve(new Keyframe[2] { new Keyframe(0f, 1.0f), new Keyframe(1f, 1f) });
            spread = 0.0f;
            delay = Vector2.zero;
            loop = false;
#if OCULUS
            ospProps = new OSPProps();
            useVirtualSpeaker = false;
#endif
        }

        [Tooltip("Each sound FX should have a unique name")]
        public string name = string.Empty;
        [Tooltip("Only can play one Ambience and one Music and an ilimited number of SFX audios at the same time.")]
        public AudioType audioType = AudioType.Ambience;
        [Tooltip("Sound diversity playback option when multiple audio clips are defined, default = Random")]
        public SoundFXNext playback = SoundFXNext.Random;

        [Tooltip("Set to true for the sound to loop continuously, default = false")]
        public bool loop = false;
        [Tooltip("Default volume for this sound FX, default = 1.0")]
        [Range(0.0f, 1.0f)]
        public float volume = 1.0f;
        [Tooltip("Random pitch variance each time a sound FX is played, default = 1.0 (none)")]
        //[MinMax(1.0f, 1.0f, 0.0f, 2.0f)]
        public Vector2 pitchVariance = Vector2.one;
        [Range(0f, 1f)]
        public float spatialBlend = 0f;
        [Tooltip("The amount by which the signal from the AudioSource will be mixed into the global reverb associated with the Reverb Zones | Valid range is 0.0 - 1.1, default = 1.0")]
        public AnimationCurve reverbZoneMix = new AnimationCurve(new Keyframe[2] { new Keyframe(0f, 1.0f), new Keyframe(1f, 1f) });

        [Header("3D Sound Settings")]
        [Tooltip("Sets the spread angle (in degrees) of a 3d stereo or multichannel sound in speaker space, default = 0")]
        [Range(0.0f, 360.0f)]
        public float spread = 0.0f;
        [Tooltip("Falloff distance for the sound FX, default = 1m min to 25m max")]
        //[MinMax(1.0f, 25.0f, 0.0f, 250.0f)]
        public Vector2 falloffDistance = new Vector2(1.0f, 25.0f);
        [Tooltip("Volume falloff curve - sets how the sound FX attenuates over distance, default = Linear")]
        public AudioRolloffMode falloffCurve = AudioRolloffMode.Linear;
        [Tooltip("Defines the custom volume falloff curve")]
        public AnimationCurve volumeFalloffCurve = new AnimationCurve(new Keyframe[2] { new Keyframe(0f, 1.0f), new Keyframe(1f, 1f) });
        [Tooltip("Specifies the default delay when this sound FX is played, default = 0.0 secs")]

        //[MinMax(0.0f, 0.0f, 0.0f, 2.0f)]
        public Vector2 delay = Vector2.zero;   // this overrides any delay passed into PlaySound() or PlaySoundAt()

#if OCULUS
        public OSPProps ospProps;

        [Header("Ambisonics")]
        public bool useVirtualSpeaker = false;
#endif

        [Tooltip("List of the audio clips assigned to this sound FX")]
        public AudioClip[] soundClips = new AudioClip[1];

        protected int m_playingIdx = -1;
        protected int m_lastIdx = -1;

        public bool IsValid => (soundClips.Length != 0) && (soundClips[0] != null);
        public int Length => soundClips.Length;

        public AudioClip Clip
        {
            get
            {
                if (soundClips.Length == 0)
                {
                    return null;
                }
                else if (soundClips.Length == 1)
                {
                    return soundClips[0];
                }
                if (playback == SoundFXNext.Random)
                {
                    // random, but don't pick the last one
                    int idx = UnityEngine.Random.Range(0, soundClips.Length);
                    while (idx == m_lastIdx)
                    {
                        idx = UnityEngine.Random.Range(0, soundClips.Length);
                    }
                    m_lastIdx = idx;
                    return soundClips[idx];
                }
                else
                {
                    // sequential
                    if (++m_lastIdx >= soundClips.Length)
                    {
                        m_lastIdx = 0;
                    }
                    return soundClips[m_lastIdx];
                }
            }
        }

        public AudioDimension AudioDimension => AudioDimension._3D;

        public float Pitch => UnityEngine.Random.Range(pitchVariance.x, pitchVariance.y);

        public float Delay => delay.y > 0 ? UnityEngine.Random.Range(delay.x, delay.y) : 0f;

        public AudioType AudioType => audioType;

        public bool Loop => loop;

        public float Volume => volume;

        public float SpatialBlend => spatialBlend;

        public AnimationCurve ReverbZoneMix => reverbZoneMix;

        public float Spread => spread;

        public Vector2 FalloffDistance => falloffDistance;

        public AudioRolloffMode FalloffCurve => falloffCurve;

        public AnimationCurve VolumeFalloffCurve => volumeFalloffCurve;

#if OCULUS
        public OSPProps OSPProps => ospProps;

        public bool UseVirtualSpeaker => useVirtualSpeaker;
#endif

        public float GetClipLength(int idx)
        {
            if ((idx == -1) || (soundClips.Length == 0) || (idx >= soundClips.Length) || (soundClips[idx] == null))
            {
                return 0.0f;
            }
            else
            {
                return soundClips[idx].length;
            }
        }

        public virtual int Play(float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual int PlayAt(Vector3 point, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual int PlayAt(Transform parent, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual bool Stop()
        {
            bool stopped = false;

            return stopped;
        }
    }

    [Serializable]
    public class Audio2DProfile_Default : IAudioProfile
    {
        [Tooltip("Only can play one Ambience and one Music and an ilimited number of SFX audios at the same time.")]
        public AudioType audioType = AudioType.SFX;
        [Tooltip("Each sound FX should have a unique name")]
        public string name = string.Empty;
        [Tooltip("Sound diversity playback option when multiple audio clips are defined, default = Random")]
        public SoundFXNext playback = SoundFXNext.Random;

        [Tooltip("Set to true for the sound to loop continuously, default = false")]
        public bool loop = false;
        [Tooltip("Default volume for this sound FX, default = 1.0")]
        [Range(0.0f, 1.0f)]
        public float volume = 1.0f;
        [Tooltip("Random pitch variance each time a sound FX is played, default = 1.0 (none)")]
        //[MinMax(1.0f, 1.0f, 0.0f, 2.0f)]
        public Vector2 pitchVariance = Vector2.one;
        [Tooltip("The amount by which the signal from the AudioSource will be mixed into the global reverb associated with the Reverb Zones | Valid range is 0.0 - 1.1, default = 1.0")]
        public AnimationCurve reverbZoneMix = new AnimationCurve(new Keyframe[2] { new Keyframe(0f, 1.0f), new Keyframe(1f, 1f) });

        [Tooltip("Specifies the default delay when this sound FX is played, default = 0.0 secs")]
        //[MinMax(0.0f, 0.0f, 0.0f, 2.0f)]
        public Vector2 delay = Vector2.zero;   // this overrides any delay passed into PlaySound() or PlaySoundAt()

        [Tooltip("List of the audio clips assigned to this sound FX")]
        public AudioClip[] soundClips = new AudioClip[1];

        protected int m_playingIdx = -1;
        protected int m_lastIdx = -1;

        public bool IsValid => (soundClips.Length != 0) && (soundClips[0] != null);
        public int Length => soundClips.Length;

        public AudioClip Clip
        {
            get
            {
                if (soundClips.Length == 0)
                {
                    return null;
                }
                else if (soundClips.Length == 1)
                {
                    return soundClips[0];
                }
                if (playback == SoundFXNext.Random)
                {
                    // random, but don't pick the last one
                    int idx = UnityEngine.Random.Range(0, soundClips.Length);
                    while (idx == m_lastIdx)
                    {
                        idx = UnityEngine.Random.Range(0, soundClips.Length);
                    }
                    m_lastIdx = idx;
                    return soundClips[idx];
                }
                else
                {
                    // sequential
                    if (++m_lastIdx >= soundClips.Length)
                    {
                        m_lastIdx = 0;
                    }
                    return soundClips[m_lastIdx];
                }
            }
        }

        public AudioDimension AudioDimension => AudioDimension._2D;

        public float Pitch => UnityEngine.Random.Range(pitchVariance.x, pitchVariance.y);

        public float Delay => delay.y > 0 ? UnityEngine.Random.Range(delay.x, delay.y) : 0f;

        public AudioType AudioType => audioType;

        public bool Loop => loop;

        public float Volume => volume;

        public float SpatialBlend => 0f;

        public AnimationCurve ReverbZoneMix => reverbZoneMix;

        public float Spread => 0;

        public Vector2 FalloffDistance => Vector2.zero;

        public AudioRolloffMode FalloffCurve => AudioRolloffMode.Linear;

        public AnimationCurve VolumeFalloffCurve => null;

#if OCULUS
        public OSPProps OSPProps => default;

        public bool UseVirtualSpeaker => false;
#endif

        public float GetClipLength(int idx)
        {
            if ((idx == -1) || (soundClips.Length == 0) || (idx >= soundClips.Length) || (soundClips[idx] == null))
            {
                return 0.0f;
            }
            else
            {
                return soundClips[idx].length;
            }
        }

        public virtual int Play(float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual int PlayAt(Vector3 point, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual int PlayAt(Transform parent, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            m_playingIdx = -1;

            if (!IsValid)
            {
                return m_playingIdx;
            }

            return m_playingIdx;
        }

        public virtual bool Stop()
        {
            bool stopped = false;

            return stopped;
        }
    }

#if OCULUS
    [System.Serializable]
    public struct OSPProps
    {
        [Tooltip("Set to true to play the sound FX spatialized with binaural HRTF, default = false")]
        public bool enableSpatialization;
        [Tooltip("Play the sound FX with reflections, default = false")]
        public bool useFastOverride;
        [Tooltip("Boost the gain on the spatialized sound FX, default = 0.0")]
        [Range(0.0f, 24.0f)]
        public float gain;
        [Tooltip("Enable Inverse Square attenuation curve, default = false")]
        public bool enableInvSquare;
        [Tooltip("Change the sound from point source (0.0f) to a spherical volume, default = 0.0")]
        [Range(0.0f, 1000.0f)]
        public float volumetric;
        [Tooltip("Set the near and far falloff value for the OSP attenuation curve, default = 1.0")]
        [MinMax(1.0f, 25.0f, 0.0f, 250.0f)]
        public Vector2 invSquareFalloff;// = new Vector2(1.0f, 25.0f);
    }
#endif
}