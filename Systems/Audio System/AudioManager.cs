namespace d4160.ECS.Component
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Audio;
    using d4160.Core;
    using System.Threading.Tasks;

    /// <summary>
    /// Has Globals Audio Components. AudioSource for Play global music and AudioMixer to control all audio
    /// By default has two MixerGroups: Music, Ambience and SFX (include voices)
    /// Can use many mixers but with the same Master, music and sfx
    /// Use different Views to get more order
    /// </summary>
    public abstract class AudioManager<T, AE> : ComponentPoolManager<T> where T : MonoBehaviour where AE : AudioEmitterComponent
    {
        [SerializeField]
        protected AE m_musicEmitter;
        [SerializeField]
        protected AE m_ambienceEmitter;
        [SerializeField]
        protected AE m_sfxEmitter;
        [SerializeField]
        protected AE m_voiceEmitter;
        [SerializeField]
        protected AudioMixer m_mainAudioMixer;
        [SerializeField]
        protected AudioMixerSnapshot m_unpausedSnapshot;
        [SerializeField]
        protected AudioMixerSnapshot m_pausedSnapshot;
        [SerializeField]
        protected float m_snapshotTransitionTime = 0.05f;
        [SerializeField]
        protected float m_audioFadeDuration = 1f;
        [Tooltip("The audio mixer group used for the pooled emitters")]
        [SerializeField]
        protected AudioMixerGroup m_defaultMixerGroup = null;
        [SerializeField]
        [Tooltip("Exposed parameter inside the mixer to control Music Volumen")]
        protected string m_musicParamName = "musicVolume";
        [Tooltip("Exposed parameter inside the mixer to control Ambience Volumen")]
        [SerializeField]
        protected string m_ambienceParamName = "ambienceVolume";
        [Tooltip("Exposed parameter inside the mixer to control SFX Volumen")]
        [SerializeField]
        protected string m_sfxParamName = "sfxVolume";
        [Tooltip("Exposed parameter inside the mixer to control Voice Volumen")]
        [SerializeField]
        protected string m_voiceParamName = "voiceVolume";

        protected List<AE> m_audioEmitters = new List<AE>();

        /* Need a list of AudioGroups */

        #region Editor members
        /// <summary>
        /// Get group names
        /// </summary>
        /// <param name="listIdx">The list idx which indicates the list to choice from (allow have different types of AudioGroup[]) (maybe for 3D and 2D)</param>
        /// <returns></returns>
        public virtual string[] GetGroupNames(int listIdx = 0) {
            return new string[0];
        }

        /// <summary>
        /// Get audio profiles names inside a group
        /// </summary>
        /// <param name="groupIdx">The group idx</param>
        /// <param name="listIdx">The list idx which indicates the list to choice from (allow have different types of AudioGroup[]) (maybe for 3D and 2D)</param>
        /// <returns></returns>
        public virtual string[] GetProfileNames(int groupIdx, int listIdx = 0) {
            return new string[0];
        }
        #endregion


        public virtual float MasterVolumen
        {
            get => AudioListener.volume;
            set
            {
                if (!MuteMaster)
                    AudioListener.volume = value;
                else
                    MasterVolumeCached = value;
            }
        }

        public virtual float MusicVolumen {
            get {
                float volumen;
                if (m_mainAudioMixer.GetFloat(m_musicParamName, out volumen))
                    return volumen;

                return -1;
            }
            set {
                m_mainAudioMixer.SetFloat(m_musicParamName, value);
            }
        }

        public virtual float AmbienceVolumen
        {
            get
            {
                float volumen;
                if (m_mainAudioMixer.GetFloat(m_ambienceParamName, out volumen))
                    return volumen;

                return -1;
            }
            set
            {
                m_mainAudioMixer.SetFloat(m_ambienceParamName, value);
            }
        }

        public virtual float SFXVolumen
        {
            get
            {
                float volumen;
                if (m_mainAudioMixer.GetFloat(m_sfxParamName, out volumen))
                    return volumen;

                return -1;
            }
            set
            {
                m_mainAudioMixer.SetFloat(m_sfxParamName, value);
            }
        }

        public virtual float VoiceVolumen
        {
            get
            {
                float volumen;
                if (m_mainAudioMixer.GetFloat(m_voiceParamName, out volumen))
                    return volumen;

                return -1;
            }
            set
            {
                m_mainAudioMixer.SetFloat(m_voiceParamName, value);
            }
        }

        public virtual bool MuteMaster { get; set; }
        public virtual bool MuteMusic { get; set; }
        public virtual bool MuteAmbience { get; set; }
        public virtual bool MuteSfx { get; set; }
        public virtual bool MuteVoice { get; set; }
        protected virtual float MasterVolumeCached { get; set; }
        protected virtual float MusicVolumeCached { get; set; }
        protected virtual float AmbienceVolumeCached { get; set; }
        protected virtual float SfxVolumeCached { get; set; }
        protected virtual float VoiceVolumeCached { get; set; }

        protected virtual void OnEnable()
        {
            m_musicEmitter.SetActive(true);
            m_ambienceEmitter.SetActive(false);
            m_sfxEmitter.SetActive(false);
        }

        /// <summary>
        /// Play clip, only for 2D space clips, also use a simple 2D AudioProfile
        /// </summary>
        /// <typeparam name="AP"></typeparam>
        /// <param name="profile"></param>
        public virtual int PlayOneShoot<AP>(AP profile, float delay = 0f, float volumeScale = 1.0f, Action onEnd = null) where AP : IAudioProfile
        {
            //m_sfxEmitter.transform.parent = m_poolData.parent;

            m_sfxEmitter.ASource.PlayOneShot(profile.Clip, volumeScale);

            m_sfxEmitter.SetActive(true);

            if (onEnd != null)
                WaitForEndOfClip(2, m_sfxEmitter.ASource.clip.length, onEnd).WrapErrors();

            return 2;
        }

        /// <summary>
        /// Play clip at point using one AudioSource of the pool
        /// </summary>
        /// <typeparam name="AP"></typeparam>
        /// <param name="profile"></param>
        /// <param name="point"></param>
        public virtual int PlayAt<AP>(AP profile, Vector3 point, float delay = 0f, float volumeOverride = 1.0f, Action onEnd = null) where AP : IAudioProfile
        {
            int idx = -1;

            for (int i = 0; i < m_audioEmitters.Count; i++)
            {
                if (!m_audioEmitters[i].IsActived)
                {
                    idx = i;
                    break;
                }
            }

            if (idx == -1)
            {
                m_audioEmitters.Add(PopObjectFromPool<AE>());
                idx = m_audioEmitters.Count - 1;
            }

            AE aEmitter = m_audioEmitters[idx];
            //aEmitter.transform.parent = m_poolData.parent;
            aEmitter.transform.position = point;

            aEmitter.ASource.clip = profile.Clip;

            aEmitter.SetActive(true);

            WaitForEndOfClip(idx, aEmitter.ASource.clip.length, onEnd).WrapErrors();

            return idx + 3;
        }

        /// <summary>
        /// Play clip at transform as parent using one AudioSource of the pool
        /// </summary>
        /// <typeparam name="AP"></typeparam>
        /// <param name="profile"></param>
        /// <param name="point"></param>
        public virtual int PlayAt<AP>(AP profile, Transform parent, float delay = 0f, float volumeOverride = 1.0f, Action onEnd = null) where AP : IAudioProfile
        {
            int idx = -1;

            for (int i = 0; i < m_audioEmitters.Count; i++)
            {
                if (!m_audioEmitters[i].IsActived)
                {
                    idx = i;
                    break;
                }
            }

            if (idx == -1)
            {
                m_audioEmitters.Add(PopObjectFromPool<AE>());
                idx = m_audioEmitters.Count - 1;
            }

            AE aEmitter = m_audioEmitters[idx];
            aEmitter.transform.parent = parent;

            aEmitter.ASource.clip = profile.Clip;

            aEmitter.SetActive(true);

            WaitForEndOfClip(idx, aEmitter.ASource.clip.length, onEnd).WrapErrors();

            // + the 3 default emitters
            return idx + 3;
        }

        /// <summary>
        /// Play clip from this transform position
        /// </summary>
        /// <typeparam name="AP"></typeparam>
        /// <param name="profile"></param>
        public virtual int Play<AP>(AP profile, float delay = 0f, float volumeOverride = 1.0f, Action onEnd = null) where AP : IAudioProfile
        {
            int idx = -1;

            for (int i = 0; i < m_audioEmitters.Count; i++)
            {
                if (!m_audioEmitters[i].IsActived)
                {
                    idx = i;
                    break;
                }
            }

            if (idx == -1)
            {
                m_audioEmitters.Add(PopObjectFromPool<AE>());
                idx = m_audioEmitters.Count - 1;
            }

            AE aEmitter = m_audioEmitters[idx];
            //aEmitter.transform.parent = m_poolData.parent;

            aEmitter.ASource.clip = profile.Clip;

            aEmitter.SetActive(true);

            WaitForEndOfClip(idx, aEmitter.ASource.clip.length, onEnd).WrapErrors();

            return idx + 3;
        }

        private async Task WaitForEndOfClip(int idx, float length, Action onEnd = null)
        {
            await TimeSpan.FromSeconds(length);

            onEnd?.Invoke();

            if (idx < 3)
            {
                switch (idx)
                {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
            }
            else
            {
                m_audioEmitters[idx].SetActive(false);
            }
        }

        /// <summary>
        /// Method to free memory when there is not need more SFX at the current time
        /// TO DO: Make a link with TimeSystem to measure a time of inutilization
        /// </summary>
        public virtual void ReturnBackAllToPool()
        {
            for (int i = 0; i < m_audioEmitters.Count; i++)
            {
                PushObjectToPool(m_audioEmitters[i]);
            }
        }

        /// <summary>
        /// Lowpass effect by default, call this after the Game Pause method (the one which change Time.scale)
        /// </summary>
        public virtual void SwitchPause()
        {
            Lowpass();
        }

        public virtual bool Stop(int idx)
        {
            if (idx < 0)
                return false;

            if (idx < 3)
            {
                switch (idx)
                {
                    case 0:
                        if (!m_musicEmitter.IsActived)
                            return false;

                        m_musicEmitter.FadeAudio(m_audioFadeDuration);
                        break;
                    case 1:
                        if (!m_ambienceEmitter.IsActived)
                            return false;

                        m_ambienceEmitter.FadeAudio(m_audioFadeDuration);
                        break;
                    case 2:
                        if (!m_sfxEmitter.IsActived)
                            return false;

                        m_sfxEmitter.FadeAudio(m_audioFadeDuration);
                        break;
                }

                return true;
            }
            else
            {
                // - the 3 default emitters
                if (!m_audioEmitters[idx - 3].IsActived)
                    return false;

                m_audioEmitters[idx - 3].FadeAudio(m_audioFadeDuration);

                return true;
            }
        }

        /// <summary>
        /// LowPass paused audio effect.
        /// Call this after the Game Pause method.
        /// </summary>
        protected virtual void Lowpass()
        {
            if (Time.timeScale == 0)
            {
                if (m_pausedSnapshot)
                    m_pausedSnapshot.TransitionTo(m_snapshotTransitionTime);
            }
            else
            {
                if (m_unpausedSnapshot)
                    m_unpausedSnapshot.TransitionTo(m_snapshotTransitionTime);
            }
        }

        public abstract IAudioProfile GetAudioProfile(int groupIdx, int profileIdx);
    }

    [Serializable]
    public class AudioGroup<T> where T : IAudioProfile
    {
        public AudioGroup(string name)
        {
            this.name = name;
        }
        public AudioGroup()
        {
            mixerGroup = null;
        }

        public string name = string.Empty;
        public AudioMixerGroup mixerGroup = null;                       // default = AudioManager.defaultMixerGroup
        public T[] audioProfiles = new T[0];
    }

    [Serializable]
    public struct AudioProfileRef
    {
        public int groupIndex;
        public int profileIndex;
        /// <summary>
        /// The Audio Profile linked to, make sure that the Entity that uses this allow fill this at Init
        /// with AudioManager(group, profile);
        /// </summary>
        public IAudioProfile profileRef;

        public int Play(float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            return profileRef.Play(delaySecs, volumeOverride, onEnd);
        }

        public int PlayAt(Vector3 point, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            return profileRef.PlayAt(point, delaySecs, volumeOverride, onEnd);
        }

        public int PlayAt(Transform parent, float delaySecs = 0.0f, float volumeOverride = 1.0f, Action onEnd = null)
        {
            return profileRef.PlayAt(parent, delaySecs, volumeOverride, onEnd);
        }

        public bool Stop()
        {
            return profileRef.Stop();
        }
    }

    public enum AudioType
    {
        Music,
        Ambience,
        SFX
    }

    public enum SoundFXNext
    {
        Random = 0,
        Sequential = 1,
    }

    public enum AudioDimension
    {
        _2D,
        _3D
    }

    public enum PreloadSounds
    {
        Default,        // default unity behavior
        Preload,        // audio clips are forced to preload
        ManualPreload,  // audio clips are forced to not preload, preloading must be done manually
    }
}
