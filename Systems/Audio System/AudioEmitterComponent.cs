﻿namespace d4160.ECS.Component
{
    using UnityEngine;

    [RequireComponent(typeof(AudioSource))]
    public class AudioEmitterComponent : MonoBehaviour
    {
        protected AudioSource m_aSource;
        //protected int m_idx;

        public AudioSource ASource {
            get {
                return m_aSource;
            }
        }

        /*public int Index {
            get {
                return m_idx;
            }
            set {
                m_idx = value;
            }
        }*/

        public bool IsActived
        {
            get
            {
                return m_aSource.enabled;
            }
        }

        protected virtual void Awake()
        {
            if (!m_aSource)
                m_aSource = GetComponent<AudioSource>();    
        }

        public virtual void SetActive(bool active)
        {
            if (!m_aSource)
                m_aSource = GetComponent<AudioSource>();

            /*if (m_aSource.isPlaying)
                m_aSource.Stop();*/

            m_aSource.enabled = active;
        }

        /// <summary>
        /// Override this to implement different logic like Coroutines, UniRX, Tween Plugin, TimeSystem, etc. Default: Coroutine
        /// </summary>
        /// <param name="toFade"></param>
        /// <param name="duration"></param>
        public virtual void FadeAudio(float duration)
        {
            SetActive(false);
        }
    }
}