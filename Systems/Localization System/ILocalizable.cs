using UnityEngine;

namespace d4160
{
    /// <summary>
    /// Interface to allow Localize Entities and Definition 
    /// </summary>
    public interface ILocalizable
    {
        void Localize(int key, string value);

        void Localize(int key, AudioClip value);
    }

    /// <summary>
    /// Interface for the object responsible to call the entities to localize
    /// Only call for Instanced Entities and Definitions, each Entity need to try to localize On Initialize
    /// </summary>
    public interface ILocalizer
    {
        void Localize(int lanId, int opIdx = 0);
    }
}
