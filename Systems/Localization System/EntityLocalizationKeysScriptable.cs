namespace d4160.Scriptables
{
    using d4160.ECS.Entity;
    using d4160.Core;
    using UnityEngine;

    //[CreateAssetMenu(fileName = "EntityLocalizationKeysScriptable", menuName = "Scriptable/Localization/EntityLocalizationKeysScriptable")]
    public class EntityLocalizationKeysScriptable : LocalizationKeysScriptable
    {
        // For each Localizable Archetype one new LocalizableData
        [SerializeField]
        protected EntityStringLocalizableData[] m_stringLocalizables;
        [SerializeField]
        protected EntityAudioLocalizableData[] m_audioLocalizables;

        /// <summary>
        /// To speed up this can imitate the arrange of the EntityScriptable, with a id, catId and a position inside cat
        /// </summary>
        /// <param name="id"></param>
        /// <param name="catId"></param>
        /// <returns></returns>
        private int GetStringLocalizableDataIdx(int id, int catId)
        {
            for (int i = 0; i < m_stringLocalizables.Length; i++)
            {
                if (m_stringLocalizables[i].id == id &&
                    m_stringLocalizables[i].catId == catId)
                {
                    return i;
                }
            }
            return -1;
        }

        private int GetAudioLocalizableDataIdx(int id, int catId)
        {
            for (int i = 0; i < m_audioLocalizables.Length; i++)
            {
                if (m_audioLocalizables[i].id == id &&
                    m_audioLocalizables[i].catId == catId)
                {
                    return i;
                }
            }
            return -1;
        }

        public override void Localize(int lanId, ILocalizable localizable)//, IEntity entity)
        {
            int idx = -1;// GetStringLocalizableDataIdx(entity.Id, entity.CategoryId);
            if (idx == -1)
                return;

            for (int i = 0; i < m_stringLocalizables[idx].stringLocalizableData.Length; i++)
            {
                localizable.Localize(m_stringLocalizables[idx].stringLocalizableData[i].key,
                    m_stringLocalizables[idx].stringLocalizableData[i].GetLocalizableValue(lanId));
            }

            //idx = GetAudioLocalizableDataIdx(entity.Id, entity.CategoryId);
            if (idx == -1)
                return;

            for (int i = 0; i < m_audioLocalizables[idx].audioLocalizableData.Length; i++)
            {
                localizable.Localize(m_audioLocalizables[idx].audioLocalizableData[i].key,
                    m_audioLocalizables[idx].audioLocalizableData[i].GetLocalizableValue(lanId));
            }
        }
    }

    [System.Serializable]
    public struct EntityStringLocalizableData
    {
        public int catId;
        public int id;
        public StringLocalizableData[] stringLocalizableData;
    }

    [System.Serializable]
    public struct EntityAudioLocalizableData
    {
        public int catId;
        public int id;
        public AudioLocalizableData[] audioLocalizableData;
    }

    [System.Serializable]
    public struct StringLocalizableData
    {
        [Tooltip("The key for this LocalizableData to comunicate correctly with the Localizable (for example key 0 means Name)")]
        public int key;
        [Tooltip("Array of string that contains values for each available language (LocalizationScriptable of GameSingleton). Need to protect the same order.")]
        public string[] localizableData;

        public string GetLocalizableValue(int lanId)
        {
            if (localizableData.IsValidIndex(lanId - 1))
            {
                return localizableData[lanId - 1];
            }

            return null;
        }
    }

    [System.Serializable]
    public struct AudioLocalizableData
    {
        public int key;
        public AudioClip[] localizableData;

        public AudioClip GetLocalizableValue(int lanId)
        {
            if (localizableData.IsValidIndex(lanId - 1))
            {
                return localizableData[lanId - 1];
            }

            return null;
        }
    }
}
