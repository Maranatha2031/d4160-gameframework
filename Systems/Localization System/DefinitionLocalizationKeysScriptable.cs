namespace d4160.Scriptables
{
    using UnityEngine;

    //[CreateAssetMenu(fileName = "DefinitionLocalizationKeysScriptable", menuName = "Scriptable/Localization/DefinitionLocalizationKeysScriptable")]
    public class DefinitionLocalizationKeysScriptable : LocalizationKeysScriptable
    {
        // For each Localizable Archetype one new LocalizableData
        [SerializeField]
        protected DefinitionStringLocalizableData[] m_stringLocalizables;

        protected DefinitionStringLocalizableData this[int idx] {
            get {
                return m_stringLocalizables[idx];
            }
        }

        private int GetDefLocalizableDataIdx(int id)
        {
            for (int i = 0; i < m_stringLocalizables.Length; i++)
            {
                if (m_stringLocalizables[i].definitionId == id)
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Localize one definition to the specified language ID
        /// </summary>
        /// <param name="lanId"></param>
        /// <param name="localizable"></param>
        /// <param name="definitionId"></param>
        public override void Localize(int lanId, ILocalizable localizable, int definitionId)
        {
            int idx = GetDefLocalizableDataIdx(definitionId);
            if (idx == -1)
                return;

            for (int i = 0; i < this[idx].stringLocalizableData.Length; i++)
            {
                localizable.Localize(this[idx].stringLocalizableData[i].key,
                    this[idx].stringLocalizableData[i].GetLocalizableValue(lanId));
            }
        }
    }

    [System.Serializable]
    public struct DefinitionStringLocalizableData
    {
        public int definitionId;
        [Tooltip("Array of keys and values for localizables")]
        public StringLocalizableData[] stringLocalizableData;
    }
}
