namespace d4160.Scriptables
{
    using d4160.ECS.Entity;
    using UnityEngine;

    // Localization related data for an EntityScriptable or DefinitionScriptable
    // Two mayor lists, one for string and the other for audioclips
    // Foreach key has a List of strings or audioclips (one for each language)

    public abstract class LocalizationKeysScriptable : ScriptableObject//, IScriptable
    {
        public virtual void Localize(int lanId, ILocalizable localizable) { }//, IEntity entity) { }

        public virtual void Localize(int lanId, ILocalizable localizable, int definitionId) { }
    }
}
