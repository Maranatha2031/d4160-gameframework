namespace d4160.Scriptables
{
    using d4160.Core;
    using UnityEngine;
    using d4160.GameFramework;

    /// <summary>
    ///
    /// </summary>
    /* [CreateAssetMenu(fileName = "LocalizationScriptable", menuName = "Scriptable/Global/Localization")] */
    public abstract class LocalizationScriptable<T, T2> //: ArchetypesSO<T> where T : LocalizationDefinition, new() where T2 : LocalizationDefinition
    {
        [SerializeField] protected T2[] m_voiceDefinitions;

        public virtual T2[] VoiceDefinitions => m_voiceDefinitions;

        public virtual T2 GetVoiceDefAt(int idx)
        {
            return m_voiceDefinitions.IsValidIndex(idx) ? m_voiceDefinitions[idx] : default;
        }

        public virtual T2 GetVoiceDefWith(int id)
        {
            /*for (var i = 0; i < m_voiceDefinitions.Length; i++)
            {
                if (m_voiceDefinitions[i].ID == id)
                    return m_voiceDefinitions[i];
            }*/

            return default(T2);
        }

        private void FixVoiceDefIds()
        {
            /*for (var i = 0; i < m_voiceDefinitions.Length; i++)
            {
                if (m_voiceDefinitions[i] != null)
                    m_voiceDefinitions[i].ID = i + 1;
            }*/
        }

        /// <summary>
        /// Try to get a unique identifier (id) if the language is available
        /// </summary>
        /// <param name="language"></param>
        /// <returns></returns>
        public abstract int GetTextIdentifier(SystemLanguage language);

        public abstract int GetVoiceIdentifier(SystemLanguage language);
    }

    [System.Serializable]
    public class LocalizationDefinition : DefaultArchetype, ILocalizable
    {
        [SerializeField] protected SystemLanguage m_language;

        public SystemLanguage Language => m_language;

        public void Localize(int key, string value)
        {
            // Only expects one 'key' for m_name, so can ignore it
            m_name = value;
        }

        public void Localize(int key, AudioClip value)
        {
        }
    }
}
