namespace d4160.ECS.Component
{
    using UnityEngine;

    /// <summary>
    /// Group of Abilities (Physical: Melee, Projectile; Weapon: Melee, Projectile; Magic/Wonder/Miracle: Proyectile (time), Instant, IAoE, ITargetSelector, IActive, IPasive, IChain (Projectile (dota linch like), Instant (zeus or tinker like)), IBuffer, IDebuffer)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AbilitySetComponent<T>// : ComponentDataWrapper<T> where T : IComponentData
    {
        // List of BaseMotions and SecondaryMotions

        public abstract void SetDestination(Vector3 targetPoint);

        public abstract void SetActiveMotion(bool active, int idx = 0);
    }
}
