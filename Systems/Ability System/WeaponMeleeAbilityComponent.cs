namespace d4160.ECS.Entity
{
    /// <summary>
    /// Player link, need to set the value of IPlayer (GameMode or Inspector variable)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="P"></typeparam>
    public abstract class WeaponMeleeAbilityComponent<EData, ETypeData> //: AbilityComponent<EData, ETypeData>, IAbility where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData
    {
    }
}