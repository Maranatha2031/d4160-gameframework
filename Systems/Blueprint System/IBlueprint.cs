namespace d4160.Systems.Blueprint
{
    public interface IBlueprint //: IEntity
    {
        void CreateBlueprint();
    }

    public interface IBlueprint<T, P> : IBlueprint where T : class
    {
        T BlueprintTarget { get; set; }

        void AddBlueprintPart(P part);
    }
}
