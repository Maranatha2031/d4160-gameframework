namespace d4160.ECS.Entity
{
    // Intermediate class for ICraftables and IPartofs
    // Since can have its own appearence 
    public abstract class BlueprintComponent<EData, ETypeData> //: EntityComponent<EData, ETypeData>, IBlueprint where EData : IEntityComponentData where ETypeData : IEntityTypeComponentData
    {
        public virtual void MakeCompleteBlueprint()
        {
            throw new System.NotImplementedException();
        }

        public virtual void ReleaseBlueprint()
        {
            throw new System.NotImplementedException();
        }

        public virtual void StepBlueprint(bool visualizeStep = true)
        {
            throw new System.NotImplementedException();
        }

        public virtual void VisualizeStepBlueprint()
        {
            throw new System.NotImplementedException();
        }
    }
}