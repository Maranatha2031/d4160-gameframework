﻿namespace d4160.Scriptables
{
    using d4160.GameFramework;

	// Are listed on RewardSet component (Achivements related to the parent Entity)
	// e.g. Completed Quest Number (in Player or Character)
	// That Player or Character also have a AchievementTracker component to track the advance of the Achievement (n/m) and the gained Achievements
	// Are like Abilities, so have a BaseClass and then each Achivement inherite from
	// For 3 stars reward, can be also a achievement since can choice the sprites, sound, etc
    //[CreateAssetMenu(fileName = "AchievementScriptable", menuName = "Scriptable/AchievementScriptable")]
    /// <summary>
    /// For more advanced storage of extra data like Icon, Localization values, etc
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AchievementScriptable<T> //: ArchetypesSO<T> where T : DefaultArchetype, new()
	{

	}
}