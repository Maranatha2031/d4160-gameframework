
namespace d4160.ECS
{
    /// <summary>
    /// Finite count of Quest, for Entities that have QuestSet (giver) or QuestTracker (tracking) components
    /// </summary>
    public interface IQuestContainer //: IEntityContainer
    {
    }
}